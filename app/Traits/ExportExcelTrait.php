<?php

namespace App\Traits;

trait ExportExcelTrait
{
    public function exportcsv($heading, $rows, $filename)
    {
        $filename = $filename . '.csv';
        ob_start();
        header('Content-Encoding: UTF-8');
        header('Content-Type: text/csv; charset=UTF-8');
        header('Content-Type: text/html; charset=UTF-8');
        header('Content-Disposition: attachment; filename=' . $filename);
        $fp = fopen('php://output', 'w');
        $firstLine = 1;

        foreach ($rows as $row) {
            if ($firstLine == 1) {
                fputcsv($fp, $heading);
                $firstLine = 2;
            }
            fputcsv($fp, $row);
        }
        fclose($fp);
        exit;
    }
}
