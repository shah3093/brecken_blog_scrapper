<?php

namespace App\Traits;

use Exception;

trait LoginTrait
{
    public function loginToRG($email, $pass)
    {
        try {
            $url = 'https://login.realgeeks.com/auth/login_action';
            $postData = ['email' => $email, 'password' => $pass];
            $this->client->request('POST', $url, $postData);

            $this->client->request('POST', $url, [
                'form_params' => $postData
            ]);

            $this->info("Login success !--");
            return true;
        } catch (Exception $ex) {
            $this->info("Login failed");
            print_r($ex->getMessage());
            return false;
        }
    }
}
