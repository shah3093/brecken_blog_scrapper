<?php

namespace App\Console\Commands;

use App\Traits\FileProcessTrait;
use Illuminate\Console\Command;
use Symfony\Component\DomCrawler\Crawler;
use Illuminate\Support\Facades\Storage;
use Exception;

class scrap_img extends Command
{
    use FileProcessTrait;
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'scrap_img:exc';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info('=========== Starting =============== ');

        $json_data = $this->load_Json('brecken', 'brecken_1583318273.json');
        $failed_img_ids = $this->load_Json('brecken', 'failed_img_ids.json');
        $failed_img = $this->create_json('brecken', 'brecken_failed_blog_img');

        foreach ($json_data as $key => $value) {
            $this->info('**** Reamining data ' . count($json_data) . " of - " . ($key + 1));

            if (in_array($value['id'], $failed_img_ids)) {
                if (!empty($value['body'])) {
                    $this->searchImage($value['body'], $value['id'], $failed_img);
                    // exit;
                }
            }
        }
    }

    public function searchImage($body, $data_id, $failed_img)
    {
        $this->info("Searching Img on ".$data_id);
        try {
            $crawler = new Crawler($body);

            $node_values = $crawler->filter('img')->each(function (Crawler $node, $i) {

                $img_src = str_replace(" ", "%20", $node->attr('src'));

                if (strpos($img_src, "http") !== false) {
                    $img_url = $img_src;
                }else{
                    $img_url ='https://www.breckenridgeassociates.com/' . $img_src;
                }

                
                return [
                    'url' => $img_url,
                    'img_name' => $node->attr('src')
                ];
            });

            foreach ($node_values as $key => $value) {

                sleep(45);

                $url = $value['url'];
                $this->info($url);
                $name = $value['img_name'];

                try {
                    $opts = array(
                        'http' =>
                        array(
                            'method'  => 'GET',
                            'timeout' => 60,
                            'user-agent' => \Campo\UserAgent::random()
                        )
                    );
                    $context  = stream_context_create($opts);

                    // $file = file_get_contents($url);

                    $file = file_get_contents($url, true, $context);
                    Storage::disk('public')->put($name, $file);
                } catch (Exception $ex) {
                    print_r($ex->getMessage());

                    $err_d = [
                        $data_id,
                        $ex->getMessage(),
                        $value['url']
                    ];

                    $this->saveJsonToFile(
                        json_encode($err_d, JSON_PRETTY_PRINT),
                        $failed_img,
                        "brecken"
                    );
                }
            }
        } catch (Exception $ex) {
            print_r($ex->getMessage());

            $this->saveJsonToFile(
                json_encode($ex->getMessage(), JSON_PRETTY_PRINT),
                $failed_img,
                "brecken"
            );
        }
    }
}
