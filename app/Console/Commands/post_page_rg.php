<?php

namespace App\Console\Commands;

use App\Traits\FileProcessTrait;
use App\Traits\LoginTrait;
use Exception;
use Illuminate\Console\Command;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7\MultipartStream;
use Symfony\Component\BrowserKit\History;
use Symfony\Component\DomCrawler\Crawler;

class post_page_rg extends Command
{
    use FileProcessTrait, LoginTrait;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'post_page_rg:exc';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    private $slugs = [];
    private $page_ids = [];
    private $current_page_id;
    public $failed_log_file = "brecken_page_failed_log_file";
    public $folder_name = "brecken/error";

    private $snippets = [];

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();

        $this->jar = new \GuzzleHttp\Cookie\CookieJar();
        $this->client = new client(['cookies' => $this->jar]);
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info('=========== Starting =============== ');

        $email = "Nathan@breckenridgeassociates.com";
        $password = "BreckAss1";

        $this->loginToRG($email, $password);

        $remain_slugs = [
            '1031-tax-deferred',
            '2014-dew-tour',
            '2016-statistics',
            '2017-statistics',
            '39-degrees-north',
            '404',
            '404-listing',
            '96',
            '97-sub'
        ];

        // $json_data = $this->load_Json('brecken/page', 'brecken_pages_1583508751.json');
        $json_data = $this->load_Json('brecken/new', 'page_brecken_1584343329.json');
        // $json_data = $this->load_Json('brecken/new', 'new_page_brecken.json');
        $this->snippets = $this->load_Json('brecken/new', 'snippets_brecken_1584342605.json');
        $this->failed_log_file = $this->create_json($this->folder_name, $this->failed_log_file);

        $i = 1;
        foreach ($json_data as $key => $data) {
            $this->info("Remainig Data " . count($json_data) . " of " . ($key + 1));

            // if ($i == 10) {
            //     exit;
            // }
            // $i++;
            // if($data['id'] == "554"){
            //     $this->current_page_id = $data['id'];
            //     $this->postToPage($data);
            //     exit;
            // }

            // sleep(3);

            if (in_array($data['file_name'], $remain_slugs)) {
                sleep(3);
                $this->current_page_id = $data['id'];
                $this->postToPage($data);
            }

            $this->info("********************");
            $this->info("********************");
        }
    }

    public function generateSlug($title, $no_new = null)
    {

        $this->info("Generating SLug");

        if (!empty($no_new)) {
            $gtitle = $title . rand(1, 10);
        } else {
            $gtitle = $title;
        }


        $slug = str_slug($gtitle, '-');

        if (in_array($slug, $this->slugs)) {
            $this->generateSlug($title);
        }

        $this->slugs[] = $slug;

        return $slug;
    }

    public function refactoringPageContent($content)
    {
        $this->info("Refacoring page content");

        $pattern = '/\#\S*#/';
        preg_match_all($pattern, $content, $matches);

        $snippet_search = [];

        // $new_content = "";

        if (!empty($matches)) {
            foreach ($matches[0] as $key => $value) {
                $snippet_content = $this->searchSnipites($value);

                if (!is_array($snippet_content)) {
                    $content = str_replace($value, $snippet_content, $content);
                } else {
                    $content = str_replace($value, "", $content);
                    $snippet_search = $snippet_content;
                }
            }
        }

        return [$content, $snippet_search];
    }


    public function searchSnipites($pattern)
    {
        $this->info("Searching Snipites...");

        $snippet_id = trim($pattern, '#');

        foreach ($this->snippets as $key => $snippet) {
            $pa = trim($pattern, "#");

            if ($snippet['snippet_id'] == $pa) {
                if (isset($snippet['code'])) {
                    return $snippet['code'];
                } else if (isset($snippet['page_limit'])) {
                    $search_id = $this->setmapping($snippet);
                    $number_of_properties = $snippet['page_limit'];
                    $listing_header = $snippet['snippet_title'];

                    $result_data = [
                        'search' => $search_id,
                        'number_of_properties' => $number_of_properties,
                        'listing_header' => $listing_header
                    ];

                    return $result_data;
                }
            }
        }
    }

    public function setmapping($snippet)
    {
        $this->info("Setting mapping");

        $search_map = [];

        //// setting minimum_sqft 

        if (isset($snippet['minimum_sqft']) && !empty($snippet['minimum_sqft'])) {
            $search_map['area_min'] =  [$snippet['minimum_sqft']];
        }

        //// setting maximum_sqft
        if (isset($snippet['maximum_sqft']) && !empty($snippet['maximum_sqft'])) {
            $search_map['area_max'] =  [$snippet['maximum_sqft']];
        }


        //// setting minimum_year
        if (isset($snippet['minimum_year']) && !empty($snippet['minimum_year'])) {
            $search_map['year_built_min'] =  [$snippet['minimum_year']];
        }

        //// setting maximum_year
        if (isset($snippet['maximum_year']) && !empty($snippet['maximum_year'])) {
            $search_map['year_built_max'] =  [$snippet['maximum_year']];
        }

        //// setting search_zip
        if (isset($snippet['search_zip']) && !empty($snippet['search_zip'])) {
            $search_map['zip'] =  [$snippet['search_zip']];
        }

        //// setting minimum_bathrooms
        if (isset($snippet['minimum_bathrooms']) && !empty($snippet['minimum_bathrooms'])) {
            $search_map['baths_min'] =  [$snippet['minimum_bathrooms']];
        }

        //// setting maximum_bathrooms
        if (isset($snippet['maximum_bathrooms']) && !empty($snippet['maximum_bathrooms'])) {
            $search_map['baths_max'] =  [$snippet['maximum_bathrooms']];
        }

        //// setting minimum_bedrooms
        if (isset($snippet['minimum_bedrooms']) && !empty($snippet['minimum_bedrooms'])) {
            $search_map['beds_min'] =  [$snippet['minimum_bedrooms']];
        }

        //// setting maximum_bedrooms
        if (isset($snippet['maximum_bedrooms']) && !empty($snippet['maximum_bedrooms'])) {
            $search_map['beds_max'] =  [$snippet['maximum_bedrooms']];
        }

        //// setting search_address
        if (isset($snippet['search_address']) && !empty($snippet['search_address'])) {
            $search_map['text'] =  [$snippet['search_address']];
        }

        //// setting search_county
        if (isset($snippet['search_county']) && !empty($snippet['search_county'])) {
            $search_map['county'] =  [$snippet['search_county']];
        }

        //// setting search_location
        if (isset($snippet['search_location']) && !empty($snippet['search_location'])) {
            $search_map['location'] =  [$snippet['search_location']];
        }

        //// setting search_city
        if (isset($snippet['search_city[0]']) && !empty($snippet['search_city[0]'])) {
            $search_map['city'] =  [$snippet['search_city[0]']];
        }

        //// setting search_city
        if (isset($snippet['search_city[1]']) && !empty($snippet['search_city[1]'])) {
            $search_map['city'] =  [$snippet['search_city[1]']];
        }
        //// setting search_city
        if (isset($snippet['search_city[2]']) && !empty($snippet['search_city[2]'])) {
            $search_map['city'] =  [$snippet['search_city[2]']];
        }
        //// setting search_city
        if (isset($snippet['search_city[3]']) && !empty($snippet['search_city[3]'])) {
            $search_map['city'] =  [$snippet['search_city[3]']];
        }
        //// setting search_city
        if (isset($snippet['search_city[4]']) && !empty($snippet['search_city[4]'])) {
            $search_map['city'] =  [$snippet['search_city[4]']];
        }
        //// setting search_city
        if (isset($snippet['search_city[5]']) && !empty($snippet['search_city[5]'])) {
            $search_map['city'] =  [$snippet['search_city[5]']];
        }
        //// setting search_city
        if (isset($snippet['search_city[6]']) && !empty($snippet['search_city[6]'])) {
            $search_map['city'] =  [$snippet['search_city[6]']];
        }
        //// setting search_city
        if (isset($snippet['search_city[9]']) && !empty($snippet['search_city[9]'])) {
            $search_map['city'] =  [$snippet['search_city[9]']];
        }
        //// setting search_city
        if (isset($snippet['search_city[11]']) && !empty($snippet['search_city[11]'])) {
            $search_map['city'] =  [$snippet['search_city[11]']];
        }

        //// setting search_city
        if (isset($snippet['search_city[11]']) && !empty($snippet['search_city[11]'])) {
            $search_map['city'] =  [$snippet['search_city[11]']];
        }

        //// setting search_c_area
        if (isset($snippet['search_c_area[0]']) && !empty($snippet['search_c_area[0]'])) {
            $search_map['city'] =  [$snippet['search_c_area[0]']];
        }

        //// setting search_c_area
        if (isset($snippet['search_c_area[1]']) && !empty($snippet['search_c_area[1]'])) {
            $search_map['city'] =  [$snippet['search_c_area[1]']];
        }

        //// setting search_c_area
        if (isset($snippet['search_c_area[3]']) && !empty($snippet['search_c_area[3]'])) {
            $search_map['city'] =  [$snippet['search_c_area[3]']];
        }
        //// setting search_c_area
        if (isset($snippet['search_c_area[6]']) && !empty($snippet['search_c_area[6]'])) {
            $search_map['city'] =  [$snippet['search_c_area[6]']];
        }

        //// setting search_c_area
        if (isset($snippet['search_subdivision']) && !empty($snippet['search_subdivision'])) {
            $search_map['neighborhood'] =   array_map('trim', explode(",", $snippet['search_subdivision']));
        }

        //// setting search_area
        if (isset($snippet['search_area']) && !empty($snippet['search_area'])) {
            $search_map['region'] =  [$snippet['search_area']];
        }

        //// setting minimum_price
        if (isset($snippet['minimum_price']) && !empty($snippet['minimum_price'])) {
            $search_map['list_price_min'] =  [$snippet['minimum_price']];
            $search_map['sold_price_min'] =  [$snippet['minimum_price']];
        }

        //// setting search_location
        if (isset($snippet['maximum_price']) && !empty($snippet['maximum_price'])) {
            $search_map['sold_price_max'] =  [$snippet['maximum_price']];
            $search_map['list_price_max'] =  [$snippet['maximum_price']];
        }

        //// setting search_type
        if (isset($snippet['search_type']) && !empty($snippet['search_type'])) {
            if ($snippet['search_type'] == "Residential") {
                $search_map['type'] = ['res'];
            } else if ($snippet['search_type'] == "Land") {
                $search_map['type'] = ['lnd'];
            }
        }

        //// setting sorting
        if (isset($snippet['sort_by']) && !empty($snippet['sort_by'])) {
            if ($snippet['sort_by'] == "DESC-ListingPrice") {
                $search_map['sort_lowest'] = [true];
            } else {
                $search_map['sort_highest'] = [true];
            }
        }

        $map = json_encode($search_map);

        $this->info($map);

        $encode_query = urlencode(json_encode($search_map));
        $url = "https://breckenridgeassociates.realgeeks.com/api/search/preview/?criteria=" . $encode_query;

        try {
            $response = $this->client->request('GET', $url);

            $obj = json_decode($response->getBody(), true);

            return $obj['data']['id_int'];
        } catch (Exception $ex) {
            print_r($ex->getMessage());


            $err_data['snippet_error'] = [
                $this->current_page_id,
                $snippet['snippet_id'],
                $search_map,
                $ex->getMessage()
            ];

            $this->saveJsonToFile(
                json_encode($err_data, JSON_PRETTY_PRINT),
                $this->failed_log_file,
                $this->folder_name
            );
        }
    }



    public function getParentId($category, $category_txt)
    {
        $this->info("Getting parent ID");

        if (strpos($category_txt, 'Set as Main Page') === false) {
            if (empty($category_txt)) {
                return null;
            }
            return $this->page_ids[$category];
        } else {
            return null;
        }
    }


    public function postToPage($data)
    {

        if (!isset($data['page_title'])) {
            return;
        }

        $csrf = $this->getCsrf();
        // $slug = $this->generateSlug($data['link_name']);
        $slug = $data['file_name'];

        $content = "";
        $snippet_search = "";
        if (isset($data['category_html'])) {
            list($content, $snippet_search) = $this->refactoringPageContent($data['category_html']);
        }

        $listing_header = "";
        $number_of_properties = "10";
        $search = "";
        if (!empty($snippet_search)) {
            $listing_header = $snippet_search['listing_header'];
            $number_of_properties = $snippet_search['number_of_properties'];
            $search = $snippet_search['search'];
        }

        // $parrent = $this->getParentId($data['category'], $data['category_txt']);
        $parrent = "";


        $post_data = [
            'csrfmiddlewaretoken' => $csrf,
            'parent' => $parrent,
            'title' => isset($data['page_title']) ? $data['page_title'] : "",
            'meta_description' => isset($data['meta_tag_desc']) ? $data['meta_tag_desc'] : "",
            'meta_keywords' => isset($data['meta_tag_keywords']) ? $data['meta_tag_keywords'] : "",
            'anchor_text' => isset($data['link_name']) ? $data['link_name'] : "",
            'slug' => $slug,
            'template' => 'Page with Search',
            'content' => $content,
            'sidebar' => '1',
            'initial-sidebar' => '1',
            'footer' => '1',
            'initial-footer' => '1',
            'capture_form' => '',
            'capture_form_first_search' => '',
            'send_autoresponder' => 'on',
            'listing_header' => $listing_header,
            'number_of_properties' => $number_of_properties,
            'property_display_location' => '0',
            'search' => $search,
            'search_header' => ''
        ];

        $params = [
            'headers' => [
                'Referer' => 'https://breckenridgeassociates.realgeeks.com/admin/content/contentpage/add/',
                'Connection' => 'keep-alive',
                'Host' => 'breckenridgeassociates.realgeeks.com',
                'Origin' => 'https://breckenridgeassociates.realgeeks.com',
                'User-Agent' => \Campo\UserAgent::random()
            ],
            'form_params' => $post_data, // here is all the magic,
            'allow_redirects' => false
        ];

        try {
            $this->info("Posting Data");
            $url = "https://breckenridgeassociates.realgeeks.com/admin/content/contentpage/add/";

            $response = $this->client->request("POST", $url, $params);

            //////Getting Page ID from cookies and saving with slug

            if (empty($parrent)) {

                $cookies = $response->getHeaders('Set-Cookie');

                $cookie_string = $cookies['Set-Cookie'][0];

                $star_str = strpos($cookie_string, 'contentpage');
                $endr_str = strpos($cookie_string, 'change');

                $star_str_len = strlen('contentpage');
                $star_str = $star_str_len + $star_str;

                $new_str = '';
                for ($i = $star_str; $i < $endr_str; $i++) {
                    $new_str .= $cookie_string[$i];
                }

                $this->page_ids[$data['category']] = trim($new_str, '/');
            }


            ///////////

            $this->info("Success !!");
        } catch (Exception $ex) {
            print_r($ex->getMessage());

            $err_data = [
                $data['id'],
                $ex->getMessage()
            ];

            $this->saveJsonToFile(
                json_encode($err_data, JSON_PRETTY_PRINT),
                $this->failed_log_file,
                $this->folder_name
            );
        }
    }



    public function getCsrf()
    {
        $this->info("Getting CSRF token");

        try {
            $base_url = "https://breckenridgeassociates.realgeeks.com/admin";
            $url = "https://breckenridgeassociates.realgeeks.com/admin/content/contentpage/add/";
            $response = $this->client->request('GET', $url);

            $response_html = (string) $response->getBody();
            $crawler = new Crawler($response_html, $base_url);

            $form = $crawler->filter('#contentpage_form')->form();
            $values = $form->getValues();

            return $values['csrfmiddlewaretoken'];
        } catch (Exception $ex) {
            print_r($ex->getMessage());
        }
    }
}
