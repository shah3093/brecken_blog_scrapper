<?php

namespace App\Console\Commands;


use App\Traits\FileProcessTrait;
use App\Traits\LoginTrait;
use Exception;
use Illuminate\Console\Command;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7\MultipartStream;
use Symfony\Component\BrowserKit\History;
use Symfony\Component\DomCrawler\Crawler;


class scrap_rg_pages extends Command
{
    use FileProcessTrait, LoginTrait;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'scrap_rg_pages:exc';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();

        $this->jar = new \GuzzleHttp\Cookie\CookieJar();
        $this->client = new client(['cookies' => $this->jar]);
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info('=========== Starting =============== ');

        $email = "featuredmarketing@gmail.com";
        $password = "Q!wfSc7aUu%y";

        $this->loginToRG($email, $password);

        $folder_name = "brecken/rg";
        $data_file_name = "rg_page";

        $data_file_name = $this->create_json($folder_name, $data_file_name);
        $rg_slugs = $this->create_json($folder_name, "rg_slugs");
        $this->scrap_page($data_file_name, $folder_name,$rg_slugs);
    }


    public function scrap_page($data_file_name, $folder_name,$rg_slugs)
    {
        $base_url = "https://www.smokymountainhomes4sale.com";
        $base_url = "https://www.anniemaloney.com";

        $data_values = [];
        
        for ($i = 0; $i < 2; $i++) {

            $this->info("Remaining page - ".($i)." from 2");
           
            $url = "https://www.anniemaloney.com/admin/content/sidebar/?p=" . $i;
            
            $response = $this->client->request('GET',$url);

            $response_html = (string) $response->getBody();

            $crawler = new Crawler($response_html, $base_url);

            $node_values = $crawler->filter("#result_list")->filter('tbody')->filter("tr")->each(function (Crawler $node, $i) {
               
                // $slug = $node->filter(".field-slug")->text();
                // $name = $node->filter(".field-anchor_text")->text();
                // $id = $node->filter('input[name="_selected_action"]')
                //                     ->extract(array('value'));
                // $parent = $node->filter(".field-parent")->text();

                // $url = "";
                // if($parent == "-"){
                //     $url = "https://breckenridgeassociates.realgeeks.com/".$slug;
                // }else{
                //     $url = "https://breckenridgeassociates.realgeeks.com".$parent.$slug;
                // }

                $sidebar_name = $node->filter(".field-name")->text();
                $sidebar_url = $node->filter(".field-name")->filter('a')->attr('href');

                // $data = [
                //     'id' => $id[0],
                //     'name' => $name,
                //     'slug' => $slug,
                //     'url' => $url 
                // ];

                $data = [
                    'Name' => $sidebar_name,
                    'URL' => $sidebar_url
                ];
                return $data;
            });

            foreach ($node_values as $key => $value) {
                array_push($data_values,$value);
            }
        }

        $this->saveJsonToFile(
            json_encode($data_values,JSON_PRETTY_PRINT),
            $data_file_name, $folder_name
        );

        $slugs = [];
        foreach ($data_values as $dkey => $dvalue) {
            // dd($dkey,$dvalue);
            $slugs[] = $dvalue['URL'];
        }

        $this->saveJsonToFile(
            json_encode($slugs,JSON_PRETTY_PRINT),
            $rg_slugs, $folder_name
        );
    }
}
