<?php

namespace App\Console\Commands;


use App\Traits\FileProcessTrait;
use App\Traits\LoginTrait;
use Exception;
use Illuminate\Console\Command;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7\MultipartStream;
use Symfony\Component\BrowserKit\History;
use Symfony\Component\DomCrawler\Crawler;

class update_page_content extends Command
{
    use FileProcessTrait, LoginTrait;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'update_page_content:exc';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    public $scraped_pages = [];

    public $failed_log_file = "brecken_page_failed_log_file";
    public $backup_content = "backup_content";
    public $folder_name = "brecken/error";

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();

        $this->jar = new \GuzzleHttp\Cookie\CookieJar();
        $this->client = new client(['cookies' => $this->jar]);
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info('=========== Starting =============== ');

        $email = "featuredmarketing@gmail.com";
        $password = "Q!wfSc7aUu%y";

        $this->loginToRG($email, $password);

        $this->failed_log_file = $this->create_json($this->folder_name, $this->failed_log_file);
        $this->backup_content = $this->create_json($this->folder_name, $this->backup_content);

        $json_data = $this->load_Json('brecken/rg', 'rg_page_1584522256.json');
        $this->scraped_pages = $json_data[0];

        $i = 1;
        foreach ($json_data[0] as $key => $data) {

            $this->info("Remainig Data " . count($json_data[0]) . " of " . ($key + 1));

            // if ($i == 20) {
            //     exit;
            // }
            // $i++;

            $this->updateContent($data);

            $this->info("********************");
            $this->info("********************");
        }
    }

    public function getCsrf()
    {
        $this->info("Getting CSRF token");

        try {
            $base_url = "https://breckenridgeassociates.realgeeks.com/admin";
            $url = "https://breckenridgeassociates.realgeeks.com/admin/content/contentpage/add/";
            $response = $this->client->request('GET', $url);

            $response_html = (string) $response->getBody();
            $crawler = new Crawler($response_html, $base_url);

            $form = $crawler->filter('#contentpage_form')->form();
            $values = $form->getValues();

            return $values['csrfmiddlewaretoken'];
        } catch (Exception $ex) {
            print_r($ex->getMessage());
        }
    }


    public function updateContent($data)
    {
        try {
            $base_url = "https://breckenridgeassociates.realgeeks.com";

            $url = "https://breckenridgeassociates.realgeeks.com/admin/content/contentpage/" . $data['id'] . "/change/";

            $this->info($url);

            $response = $this->client->request(
                'GET',
                $url
            );

            $response_html = (string) $response->getBody();

            $crawler = new Crawler($response_html, $base_url);

            $form = $crawler->filter('#contentpage_form')->form();
            $values = $form->getValues();
        } catch (Exception $ex) {
            print_r($ex->getMessage());
            return;
        }

        ///backup content

        $con_data = [
            $data['id'],
            $values['content']
        ];

        $this->saveJsonToFile(
            json_encode($con_data, JSON_PRETTY_PRINT),
            $this->backup_content,
            $this->folder_name
        );


        ///backup content

        $csrf = $this->getCsrf();

        $content = "";
        // if (isset($values['content'])) {
        //     $values['content'] = str_replace("/idx/register.html", "/member/signup/", $values['content']);
        //     // $values['content'] = str_replace(
        //     //     "//www.breckenridgeassociates.com",
        //     //     "//breckenridgeassociates.realgeeks.com",
        //     //     $values['content']
        //     // );
        // }

        // if (isset($values['content'])) {
        //     $values['content'] = str_replace(".php", "", $values['content']);
        //     $values['content'] = str_replace(
        //         "//www.breckenridgeassociates.com",
        //         "//breckenridgeassociates.realgeeks.com",
        //         $values['content']
        //     );
        // }

        // if(isset($values['parent']) && !empty($values['parent'])){
        //     $values['parent'] = "";
        // }

        ////// Updating Contents 

        $content = $values['content'];

        $slugs = $this->load_Json('brecken/rg', 'rg_slugs_1585042368.json')[0];

        $crawler = new Crawler($content);

        $node_values = $crawler->filter('a')->each(function (Crawler $node, $i) use ($slugs) {
            $n_links = $node->attr('href');


            if (strpos($n_links, '.pdf') !== false) {
               
                $new_pdf = trim($n_links);
                $new_pdf = str_replace(" ","_",$new_pdf);
                $new_pdf = str_replace("%20","_",$new_pdf);
                $new_pdf_link = "https://u.realgeeks.media/breckenridgeassociates".$new_pdf;
                $da = [
                    'link' => $n_links,
                    'rg_slug' => $new_pdf_link
                ];
            } else {
                $da = $node->attr('href');
            }

            // if (strpos($n_links, '/agents/') !== false) {
            //     $da = [
            //         'link' => $n_links,
            //         'rg_slug' => '//'
            //     ];
            // } else if (strpos($n_links, 'https://www.breckenridgeassociates.com') !== false) {
            //     $old_n_links = $n_links;
            //     $n_links = str_replace('.php', '', $n_links);
            //     $rg_slug = str_replace(
            //         "https://www.breckenridgeassociates.com",
            //         "https://breckenridgeassociates.realgeeks.com",
            //         $n_links
            //     );
            //     $da = [
            //         'link' => $old_n_links,
            //         'rg_slug' => $rg_slug
            //     ];
            // } else if (strpos($n_links, 'https://breckenridgeassociates.com') !== false) {
            //     $old_n_links = $n_links;
            //     $n_links = str_replace('.php', '', $n_links);
            //     $rg_slug = str_replace(
            //         "https://breckenridgeassociates.com",
            //         "https://breckenridgeassociates.realgeeks.com",
            //         $n_links
            //     );
            //     $da = [
            //         'link' => $old_n_links,
            //         'rg_slug' => $rg_slug
            //     ];
            // } else if (strpos($n_links, '.php') !== false) {
            //     $old_n_links = $n_links;
            //     $n_links = str_replace('.php', '', $n_links);
            //     $rg_slug = str_replace(
            //         "https://www.breckenridgeassociates.com",
            //         "https://breckenridgeassociates.realgeeks.com",
            //         $n_links
            //     );
            //     $da = [
            //         'link' => $old_n_links,
            //         'rg_slug' => $rg_slug
            //     ];
            // } else {
            //     $da = $node->attr('href');
            // }



            return $da;
        });

        // $node_values = $crawler->filter('a')->each(function (Crawler $node, $i) use ($slugs) {
        //     // return $node->attr('href');

        //     $trim_link = trim(trim($node->attr('href'), '/'));
        //     $trim_text = strtolower(trim($node->text));

        //     // if (in_array($trim_link, $slugs)) {
        //     //     $da = [
        //     //         'link' => $node->attr('href'),
        //     //         'rg_slug' => '/' . $trim_link
        //     //     ];
        //     // } else {
        //     //     $da = $node->attr('href');
        //     // }

        //     if ($trim_link == "copper-mountain") {
        //         $da = [
        //             'link' => $node->attr('href'),
        //             'rg_slug' => '/' . $trim_link
        //         ];
        //     } else if ($trim_link == "breckenridge") {
        //         $da = [
        //             'link' => $node->attr('href'),
        //             'rg_slug' => '/' . $trim_link
        //         ];
        //     } else if ($trim_text == "copper mountain") {
        //         $da = [
        //             'link' => $node->attr('href'),
        //             'rg_slug' => '/' . $trim_link
        //         ];
        //     } else if ($trim_text == "breckenridge") {
        //         $da = [
        //             'link' => $node->attr('href'),
        //             'rg_slug' => '/' . $trim_link
        //         ];
        //     } else {
        //         $da = $node->attr('href');
        //     }

        //     return $da;
        // });


        foreach ($node_values as $key => $lvalue) {
            if (isset($lvalue['link'])) {
                $href = 'href="' . $lvalue['link'] . '"';
                $re_href = 'class="popup" href="' . $lvalue['rg_slug'] . '"';

                $content = str_replace($href, $re_href, $content);
            }
        }

        $values['content'] = $content;
        ////// Updating Contents 


        $post_data = $values;
        $params = [
            'headers' => [
                'Referer' => 'https://breckenridgeassociates.realgeeks.com/admin/content/contentpage/' . $data['id'] . '/change',
                'Connection' => 'keep-alive',
                'Host' => 'breckenridgeassociates.realgeeks.com',
                'Origin' => 'https://breckenridgeassociates.realgeeks.com',
                'User-Agent' => \Campo\UserAgent::random()
            ],
            'form_params' => $post_data, // here is all the magic,
        ];


        try {
            $this->info("Updating Data");
            $url = "https://breckenridgeassociates.realgeeks.com/admin/content/contentpage/" . $data['id'] . "/change/";

            $response = $this->client->request("POST", $url, $params);

            $this->info("Success !!");
        } catch (Exception $ex) {
            print_r($ex->getMessage());

            $err_data = [
                $data['id'],
                $ex->getMessage()
            ];

            $this->saveJsonToFile(
                json_encode($err_data, JSON_PRETTY_PRINT),
                $this->failed_log_file,
                $this->folder_name
            );
        }
    }
}
