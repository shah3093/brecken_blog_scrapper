<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Traits\FileProcessTrait;

class grab_snippets extends Command
{
    use FileProcessTrait;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'grab_snippets:exc';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        $file_name = $this->create_json('brecken/polygon', 'polygon');

        $snippets = $this->load_Json('brecken/snippets', 'brecken_snippets_1583915188.json');
        $pages = $this->load_Json('brecken/new', 'page_brecken_1584343329.json');


        foreach ($pages as $key => $page) {

            $content = $page['category_html'];
            $pattern = '/\#\S*#/';
            preg_match_all($pattern, $content, $matches);
            if (!empty($matches)) {
                foreach ($matches[0] as $key => $value) {

                    $snippets_info = [];
                    $snippets_info['polygon'] = [];
                    $snippets_info['search_subtype'] = [];

                    $this->info("Searching Snipites...");

                    $snippet_id = trim($value, '#');

                    foreach ($snippets as $key => $snippet) {
                        if ($snippet['snippet_id'] == $snippet_id) {
                            if (isset($snippet['page_limit'])) {
                                if (isset($snippet['map[polygon]']) && !empty($snippet['map[polygon]'])) {
                                    array_push($snippets_info['polygon'], $snippet['map[polygon]']);
                                }

                                if (isset($snippet['search_subtype[0]']) || isset($snippet['search_subtype[3]'])) {

                                    $subtype = "";
                                    if (isset($snippet['search_subtype[0]'])) {
                                        $subtype = "all";
                                    } else {
                                        $subtype = "Townhouse";
                                    }

                                    array_push($snippets_info['search_subtype'], $subtype);
                                }
                            }
                        }
                    }

                    if(!empty($snippets_info['search_subtype']) || !empty($snippets_info['polygon'])){
                        $data = [
                            'slug' => $page['file_name'],
                            'snippet_id' => $snippet_id,
                            'snippets_info' => $snippets_info
                        ];
    
                        $data = json_encode($data, JSON_PRETTY_PRINT);
    
                        $this->saveJsonToFile($data, $file_name, 'brecken/polygon');
                    }

                }
            }
        }
    }
}
