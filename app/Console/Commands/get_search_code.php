<?php

namespace App\Console\Commands;

use App\Traits\FileProcessTrait;
use App\Traits\LoginTrait;
use Exception;
use Illuminate\Console\Command;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7\MultipartStream;
use Symfony\Component\BrowserKit\History;
use Symfony\Component\DomCrawler\Crawler;

class get_search_code extends Command
{
    use FileProcessTrait, LoginTrait;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'get_search_code:exc';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    public $county_lists = [];
    public $original_boards = [];
    public $county_list_original_board = [];

    public $backup_content = "polygon";
    public $folder_name = "brecken/polygon";

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->jar = new \GuzzleHttp\Cookie\CookieJar();
        $this->client = new client(['cookies' => $this->jar]);
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info('=========== Starting =============== ');

        $email = "featuredmarketing@gmail.com";
        $password = "Q!wfSc7aUu%y";

        $this->loginToRG($email, $password);

        $this->backup_content = $this->create_json($this->folder_name, $this->backup_content);


        // $this->getOriginalBoard();
        // $this->getCountyLists();
        // $this->getCityLists();
        $this->getAllCityLists();
    }

    public function getOriginalBoard()
    {
        $this->info("Getting All Original Board Lists");

        $search_status = [
            'search_status' => ['__VALID__']
        ];

        $criteria = [
            'attribute' => 'original_board',
            'criteria' => json_encode($search_status),
            'format' => 'json'
        ];

        $criteria_lists = http_build_query($criteria);

        $this->original_boards = $this->search_content($criteria_lists);
    }

    public function getAllCityLists()
    {
        $this->info("Getting All City Lists");

        $search_status = [
            'search_status' => ['__VALID__']
        ];

        $criteria = [
            'attribute' => 'city',
            'criteria' => json_encode($search_status),
            'format' => 'json'
        ];

        $criteria_lists = http_build_query($criteria);

        $cities = $this->search_content($criteria_lists);

        // dd($cities);

        // $this->getZipCodes($cities,$value);

        $this->getBuldings($cities);
        // $this->getSubdivisions($cities);
    }

    public function getBuldings($cities)
    {
        foreach ($cities as $key => $city) {


            $this->info("City " . $city);

            $search_status = [
                'city' => [$city],
                'search_status' => ['__VALID__']
            ];

            $criteria = [
                'attribute' => 'building_name',
                'criteria' => json_encode($search_status),
                'format' => 'json'
            ];

            $criteria_lists = http_build_query($criteria);

            $buildings = $this->search_content($criteria_lists);

            $data = [
                'city' => $city,
                'buildings' => $buildings
            ];

            $this->saveJsonToFile(
                json_encode($data, JSON_PRETTY_PRINT),
                $this->backup_content,
                $this->folder_name
            );
        }
    }


    public function getCityLists()
    {
        $this->info("Getting City Lists");

        foreach ($this->county_lists as $key => $value) {

            $this->info("Total county lists " . count($this->county_lists) . " -- search done " . ($key + 1));

            $this->info("County " . $value);

            $search_status = [
                'county' => [$value],
                'search_status' => ['__VALID__']
            ];

            $criteria = [
                'attribute' => 'city',
                'criteria' => json_encode($search_status),
                'format' => 'json'
            ];

            $criteria_lists = http_build_query($criteria);

            $cities = $this->search_content($criteria_lists);

            // $this->getZipCodes($cities,$value);

            $this->getSubdivisions($cities, $value);
        }
    }

    public function getSubdivisions($cities, $county = null)
    {
        foreach ($cities as $key => $city) {


            $this->info("City " . $city);

            $search_status = [
                'county' => [$county],
                'original_board' => [$this->county_list_original_board[$county]],
                'city' => [$city],
                'search_status' => ['__VALID__']
            ];

            $criteria = [
                'attribute' => 'subdivision',
                'criteria' => json_encode($search_status),
                'format' => 'json'
            ];

            $criteria_lists = http_build_query($criteria);

            $subdivision = $this->search_content($criteria_lists);

            $data = [
                'original_board' => $this->county_list_original_board[$county],
                'county' => $county,
                'city' => $city,
                'subdivision' => $subdivision
            ];

            dd($data);

            $this->saveJsonToFile(
                json_encode($data, JSON_PRETTY_PRINT),
                $this->backup_content,
                $this->folder_name
            );
        }
    }

    public function getZipCodes($cities, $county)
    {
        foreach ($cities as $key => $city) {


            $this->info("City " . $city);

            $search_status = [
                'county' => [$county],
                'city' => [$city]
                // 'search_status' => ['__VALID__']
            ];

            $criteria = [
                'attribute' => 'zip',
                'criteria' => json_encode($search_status),
                'format' => 'json'
            ];

            $criteria_lists = http_build_query($criteria);

            $zip_lists = $this->search_content($criteria_lists);

            $data = [
                'county' => $county,
                'city' => $city,
                'zip_lists' => $zip_lists
            ];

            $this->saveJsonToFile(
                json_encode($data, JSON_PRETTY_PRINT),
                $this->backup_content,
                $this->folder_name
            );
        }
    }

    public function getCountyLists()
    {
        $this->info("Getting County Lists");

        try {

            if (empty($this->original_boards)) {
                $search_status = [
                    'search_status' => ['__VALID__']
                ];

                $criteria = [
                    'attribute' => 'county',
                    'criteria' => json_encode($search_status),
                    'format' => 'json'
                ];

                $criteria_lists = http_build_query($criteria);

                $this->county_lists = $this->search_content($criteria_lists);
            } else {
                foreach ($this->original_boards as $original_boards) {
                    $search_status = [
                        'search_status' => ['__VALID__'],
                        'original_board' => [$original_boards]
                    ];

                    $criteria = [
                        'attribute' => 'county',
                        'criteria' => json_encode($search_status),
                        'format' => 'json'
                    ];

                    $criteria_lists = http_build_query($criteria);

                    $county_lists[$original_boards] = $this->search_content($criteria_lists);
                }

                foreach ($county_lists as $key=>$county_list) {
                    foreach ($county_list as  $value) {
                        array_push($this->county_lists, $value);
                        $this->county_list_original_board[$value] = $key;
                    }
                }
            }
        } catch (Exception $ex) {
            print_r($ex->getMessage());
        }
    }

    public function search_content($criteria_lists)
    {
        try {

            // $base_url = "https://www.davidsellsdenver.com/";

            // $base_url = "https://stlucieflrealestate.realgeeks.com/";

            // $base_url = "https://www.mainlinehomes.com/";

            // $base_url = "https://www.herbrealestate.com/";

            // $base_url = "https://www.lorykim.com/";

            // $base_url = "https://www.anniemaloney.com/";
            
            $base_url = "https://mauipropety.realgeeks.com/";

            $url = $base_url . "api/search/distinct/?" . $criteria_lists;

            // $this->info($url);

            $response = $this->client->request('GET', $url);

            $response = json_decode($response->getBody(), true);
            return $response['data'];
        } catch (Exception $ex) {
            print_r($ex->getMessage());
        }
    }
}
