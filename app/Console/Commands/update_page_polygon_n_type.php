<?php

namespace App\Console\Commands;

use App\Traits\FileProcessTrait;
use App\Traits\LoginTrait;
use Exception;
use Illuminate\Console\Command;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7\MultipartStream;
use Symfony\Component\BrowserKit\History;
use Symfony\Component\DomCrawler\Crawler;

class update_page_polygon_n_type extends Command
{
    use FileProcessTrait, LoginTrait;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'update_page_polygon_n_type:exc';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    private $slugs = [];
    private $snippets = [];
    private $page_ids = [];
    private $current_page_id;
    public $failed_log_file = "brecken_page_failed_log_file";
    public $folder_name = "brecken/error";
    public $polygon_request = "polygon_request";

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();

        $this->jar = new \GuzzleHttp\Cookie\CookieJar();
        $this->client = new client(['cookies' => $this->jar]);
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info('=========== Starting =============== ');

        $email = "featuredmarketing@gmail.com";
        $password = "Q!wfSc7aUu%y";

        $this->loginToRG($email, $password);


        $json_data = $this->load_Json('brecken/rg', 'rg_page_1585042368.json');
        $this->snippets = $this->load_Json('brecken/polygon', 'polygon_1585143157.json');
        $this->failed_log_file = $this->create_json($this->folder_name, $this->failed_log_file);
        $this->polygon_request = $this->create_json('brecken/polygon', $this->polygon_request);


        $i = 1;
        foreach ($json_data[0] as $key => $data) {
            $this->info("Remainig Data " . count($json_data) . " of " . ($key + 1));

            // if ($i == 20) {
            //     exit;
            // }
            // $i++;


            $this->updateContent($data);
            // exit;


            $this->info("********************");
            $this->info("********************");
        }
    }

    public function getCsrf()
    {
        $this->info("Getting CSRF token");

        try {
            $base_url = "https://breckenridgeassociates.realgeeks.com/admin";
            $url = "https://breckenridgeassociates.realgeeks.com/admin/content/contentpage/add/";
            $response = $this->client->request('GET', $url);

            $response_html = (string) $response->getBody();
            $crawler = new Crawler($response_html, $base_url);

            $form = $crawler->filter('#contentpage_form')->form();
            $values = $form->getValues();

            return $values['csrfmiddlewaretoken'];
        } catch (Exception $ex) {
            print_r($ex->getMessage());
        }
    }


    public function updateContent($data)
    {
        // $data['id'] = "295";

        try {
            $base_url = "https://breckenridgeassociates.realgeeks.com";

            $url = "https://breckenridgeassociates.realgeeks.com/admin/content/contentpage/" . $data['id'] . "/change/";

            $this->info($url);

            $response = $this->client->request(
                'GET',
                $url
            );

            $response_html = (string) $response->getBody();

            $crawler = new Crawler($response_html, $base_url);

            $form = $crawler->filter('#contentpage_form')->form();
            $values = $form->getValues();
        } catch (Exception $ex) {
            print_r($ex->getMessage());
            return;
        }


        $prev_search_data = [];
        if (!empty($values['search'])) {
            $prev_search_data = $this->search_display_option($data['id'], $values['search']);
        }

        $search_id = $this->update_display_option($data['id'], $values['slug'], $prev_search_data);


        if (!empty($search_id)) {
            $csrf = $this->getCsrf();
            $search = $search_id;

            $values['search'] = $search;
            $values['csrfmiddlewaretoken'] = $csrf;

            $post_data = $values;


            $params = [
                'headers' => [
                    'Referer' => 'https://breckenridgeassociates.realgeeks.com/admin/content/contentpage/633/change',
                    'Connection' => 'keep-alive',
                    'Host' => 'breckenridgeassociates.realgeeks.com',
                    'Origin' => 'https://breckenridgeassociates.realgeeks.com',
                    'User-Agent' => \Campo\UserAgent::random()
                ],
                'form_params' => $post_data, // here is all the magic,
            ];

            try {
                $this->info("Posting Data");
                $url = "https://breckenridgeassociates.realgeeks.com/admin/content/contentpage/" . $data['id'] . "/change/";

                $response = $this->client->request("POST", $url, $params);

                $this->info("Success !!");


                $dd = [
                    'url' => $url,
                    'slug' => $values['slug']
                ];

                $this->saveJsonToFile(
                    json_encode($dd, JSON_PRETTY_PRINT),
                    $this->polygon_request,
                    "brecken/polygon"
                );

            } catch (Exception $ex) {
                print_r($ex->getMessage());

                $err_data = [
                    $data['id'],
                    $ex->getMessage()
                ];

                $this->saveJsonToFile(
                    json_encode($err_data, JSON_PRETTY_PRINT),
                    $this->failed_log_file,
                    $this->folder_name
                );
            }
        }
    }


    public function update_display_option($content_id, $content_slug, $search_content)
    {


        // $this->info($content_id);
        // $this->info($content_slug);

        foreach ($this->snippets as $key => $snippet) {
            if ($snippet['slug'] == $content_slug) {

                // if (isset($search_content['type'])) {
                //     unset($search_content['type']);
                // }

                $polygon = [];
                if (!empty($snippet['snippets_info']['polygon'])) {

                    $polygon['polygon'] = [];
                    $polygon_str = "";
                    $polygon_arr = [];

                    $da = json_decode($snippet['snippets_info']['polygon'][0]);
                    $coordinators = explode(",",$da[0]);

                    
                    for ($i=0; $i < 4; $i++) {
                        $coordinats = str_replace(" ",",",$coordinators[$i]);
                        $polygon_str .= $coordinats.";";
                    }
                    $polygon_str = trim($polygon_str,";");
                    
                    // array_push($polygon_arr,$polygon_str);

                    // $polygon_arr = json_encode($polygon_arr);

                    array_push($polygon['polygon'],$polygon_str);
                }

                if (!empty($polygon)) {
                    if (isset($search_content['city'])) {
                        unset($search_content['city']);
                    }
                    if (isset($search_content['neighborhood'])) {
                        unset($search_content['neighborhood']);
                    }
                }

                if (empty($polygon)) {
                    return;
                }

                // $type = [];
                // if (!empty($snippet['snippets_info']['search_subtype'])) {
                //     $subtype = $snippet['snippets_info']['search_subtype'];

                //     if ($subtype[0] == "all") {
                //         $type['type'] = ["res", "con", "man", "hrs", "twn"];
                //     } else if ($subtype[0] == "Townhouse") {
                //         $type['type'] = ["twn"];
                //     }
                // }

                // $map = array_merge($search_content, $type, $polygon);
                $map = array_merge($search_content, $polygon);

                $search_map = json_encode($map);

                $this->info("Updating Display option");

                $this->info($search_map);

                $encode_query = urlencode(json_encode($map));
                $url = "https://breckenridgeassociates.realgeeks.com/api/search/preview/?criteria=" . $encode_query;

                try {
                    $response = $this->client->request('GET', $url);

                    $obj = json_decode($response->getBody(), true);

                    return $obj['data']['id_int'];
                } catch (Exception $ex) {
                    print_r($ex->getMessage());


                    $err_data['snippet_error'] = [
                        $content_id,
                        $snippet['snippet_id'],
                        $search_map,
                        $ex->getMessage()
                    ];

                    $this->saveJsonToFile(
                        json_encode($err_data, JSON_PRETTY_PRINT),
                        $this->failed_log_file,
                        $this->folder_name
                    );

                    return null;
                }
            }
        }
    }

    public function search_display_option($content_id, $search_id)
    {
        $param = http_build_query([
            'search_id' => '#' . $search_id,
            'format' => 'json'
        ]);

        $url = "https://breckenridgeassociates.realgeeks.com/api/search/criteria/?" . $param;

        try {

            $this->info("Searching Display option");

            $response = $this->client->request('GET', $url);

            $response = json_decode($response->getBody(), true);

            $data = $response['data'];

            if (isset($data['original_board'])) {
                unset($data['original_board']);
            }

            return $data;
        } catch (Exception $ex) {
            print_r($ex->getMessage());

            $err_data = [
                "content_id" => $content_id,
                $ex->getMessage()
            ];

            $this->saveJsonToFile(
                json_encode($err_data, JSON_PRETTY_PRINT),
                $this->failed_log_file,
                $this->folder_name
            );
        }
    }
}
