<?php

namespace App\Console\Commands;

use App\Traits\FileProcessTrait;
use Illuminate\Console\Command;
use Symfony\Component\DomCrawler\Crawler;

class grab_key extends Command
{
    use FileProcessTrait;
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'grab_key:exc';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    public $keys = [];

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $json_data = $this->load_Json('brecken/page', 'brecken_pages_1583508751.json');

        // foreach ($json_data as  $values) {
        //     if (isset($values['page_limit'])) {
        //         foreach ($values as $key => $value) {
        //             if (strpos($key, 'panels') === false) {
        //                 if (!in_array($key, $this->keys)) {
        //                     array_push($this->keys, $key);
        //                 }
        //             }
        //         }
        //     }
        // }

        foreach ($json_data as $key => $value) {

            if(!isset($value['category_html'])){
                continue;
            }

            $crawler = new Crawler($value['category_html']);

            $node_values = [];

            $node_values = $crawler->filter('a')->each(function (Crawler $node, $i) {
                $n_links = $node->attr('href');

                if (strpos($n_links, '.pdf') !== false) {
                    return $n_links;
                }
            });

            foreach ($node_values as  $node_value) {
                if(!empty($node_value)){
                    // dd($node_value);

                    $d = [
                        $node_value,
                        $value['file_name']
                    ];
                    array_push($this->keys,$d);
                    // foreach ($node_value as $rvalue) {
                    //     if (!empty($rvalue)) {
                    //         array_push($this->keys, $rvalue);
                    //     }
                    // }
                }
                
            }
        }

        print_r($this->keys);
    }
}
