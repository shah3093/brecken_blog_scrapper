<?php

namespace App\Console\Commands;

use App\Traits\FileProcessTrait;
use App\Traits\LoginTrait;
use Exception;
use Illuminate\Console\Command;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7\MultipartStream;
use Symfony\Component\BrowserKit\History;
use Symfony\Component\DomCrawler\Crawler;

class create_content extends Command
{
    use FileProcessTrait, LoginTrait;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'create_content:exc';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';
    public $clone_content = [];

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();

        $this->jar = new \GuzzleHttp\Cookie\CookieJar();
        $this->client = new client(['cookies' => $this->jar]);
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info('=========== Starting =============== ');

        $email = "featuredmarketing@gmail.com";
        $password = "Q!wfSc7aUu%y";

        $this->loginToRG($email, $password);

        $this->clone_content = $this->getCloneContent();

        // dd($this->clone_content);

        $this->copyNsaveContent();
    }

    public function search_display_option($search_id)
    {
        $param = http_build_query([
            'search_id' => '#' . $search_id,
            'format' => 'json'
        ]);

        $url = "https://www.smokymountainhomes4sale.com/api/search/criteria/?" . $param;

        try {

            $this->info("Searching Display option");

            $response = $this->client->request('GET', $url);

            $response = json_decode($response->getBody(), true);

            $data = $response['data'];

            if (isset($data['original_board'])) {
                unset($data['original_board']);
            }

            return $data;
        } catch (Exception $ex) {
            print_r($ex->getMessage());
        }
    }

    public function createSearch($city)
    {
        $search_content = $this->clone_content['search_content'];
        
        unset($search_content['exterior_finish']);

        unset($search_content['city']);
        $search_content['city'][0] = $city;

        $search_content['type'] = ['res'];

        $map = $search_content;

        $search_map = json_encode($map);

        $this->info("Creating Display option");

        $this->info($search_map);

        $encode_query = urlencode(json_encode($map));
        $url = "https://www.smokymountainhomes4sale.com/api/search/preview/?criteria=" . $encode_query;

        try {
            $response = $this->client->request('GET', $url);

            $obj = json_decode($response->getBody(), true);

            return $obj['data']['id_int'];
        } catch (Exception $ex) {
            print_r($ex->getMessage());

            return null;
        }
    }

    public function searchFieldDefault($city)
    {
        $this->info("Creating Search Field option");

        $data = [
            'city' => [$city],
            'list_price_min' => ["50000"],
            "type" => ["res", "lnd"]
        ];

        $param = http_build_query([
            'criteria' => json_encode($data),
            'format' => 'json'
        ]);

        $url = "https://www.smokymountainhomes4sale.com/api/search/preview/?" . $param;

        try {
            $response = $this->client->request('GET', $url);

            $obj = json_decode($response->getBody(), true);

            return $obj['data']['id_int'];
        } catch (Exception $ex) {
            print_r($ex->getMessage());
        }
    }

    public function copyNsaveContent()
    {

        $cities = [
            // 'Andrews',
            'Balsam',
            'Barkers Creek',
            'Bryson City',
            'Canada',
            'Canton',
            'Cashiers',
            'Cherokee (Jackson Co.)',
            'Cherokee (Swain Co.)',
            'Clayton, Ga',
            'Clyde',
            'Cullowhee',
            'Dillard, Ga',
            'Dillsboro',
            'Fontana Dam',
            'Franklin',
            'Glenville',
            'Hayesville',
            'Highlands',
            'Maggie Valley',
            'Murphy',
            'Other',
            'Otto',
            'Qualla',
            'Robbinsville',
            'Scaly Mountain',
            'Sky Valley',
            'Stecoah',
            'Sylva',
            'Topton',
            'Tuckasegee',
            'Waynesville',
            'Webster',
            'Whittier'
        ];

        $sidebar = [
            'Almond' => 5,
            'Andrews' => 6,
            'Balsam' => 55,
            'Barkers Creek' => 56,
            'Bryson City' => 88,
            'Canada' => 58,
            'Canton' => 59,
            'Cashiers' => 60,
            'Cherokee (Jackson Co.)' => 86,
            'Cherokee (Swain Co.)' => 87,
            'Clayton, Ga' => 61,
            'Clyde' => 62,
            'Cullowhee' => 63,
            'Dillard, Ga' => 64,
            'Dillsboro' => 65,
            'Fontana Dam' => 66,
            'Franklin' => 67,
            'Glenville' => 68,
            'Hayesville' => 69,
            'Highlands' => 70,
            'Maggie Valley' => 71,
            'Murphy' => 72,
            'Other' => 73,
            'Otto' => 74,
            'Qualla' => 75,
            'Robbinsville' => 76,
            'Scaly Mountain' => 77,
            'Sky Valley' => 78,
            'Stecoah' => 79,
            'Sylva' => 80,
            'Topton' => 81,
            'Tuckasegee' => 82,
            'Waynesville' => 83,
            'Webster' => 84,
            'Whittier' => 85,
        ];

        $previous_city = [
            'About Jonathan Tharp' => 8,
            'Almond' => 89,
            'Andrews' => 92,
            'Balsam' => 93,
            'Barkers Creek' => 94,
            'Bryson City' => 95,
            'Buying' => 7,
            'Canada' => 96,
            'Canton' => 97,
            'Cashiers' => 98,
            'Cherokee (Jackson Co.)' => 99,
            'Cherokee (Swain Co.)' => 100,
            'Clayton, Ga' => 101,
            'Clyde' => 102,
            'Cullowhee' => 103,
            'Dillard, Ga' => 104,
            'Dillsboro' => 105,
            'Featured properties' => 17,
            'Fontana Dam' => 106,
            'Area Foreclosures' => 5,
            'Franklin 150 200' => 27,
            'Franklin' => 107,
            'franklin sold' => 31,
            'Glenville' => 108,
            'Hayesville' => 109,
            'Highlands' => 110,
            'Jackson County' => 72,
            'Jon Tharp' => 9,
            'Almond NC Log Cabins' => 87,
            'Maggie Valley' => 111,
            'Motivated Sellers in Franklin' => 47,
            'Murphy' => 112,
            'Newsletter' => 39,
            'Other' => 113,
            'Otto' => 114,
            'Qualla' => 115,
            'Robbinsville' => 116,
            'Scaly Mountain' => 117,
            'Selling' => 6,
            'Sky Valley' => 118,
            'Smokies Investing' => 84,
            'Stecoah' => 119,
            'Sylva' => 120,
            'test_featured' => 46,
            'Topton' => 121,
            'Tuckasegee' => 122,
            'Value Property' => 45,
            'Waynesville' => 123,
            'Webster' => 124,
            'Whittier' => 125
        ];

        $cities = [
            'Almond'
        ];

        $small_char_type = "single family homes";
        $capital_char_type = "Single Family Homes";
        $type_slug = "single-family";

        foreach ($cities as $key => $city) {

            $this->info("Saving City " . $city);

            $city_slug = str_slug($city);
            $city_query_string = str_replace(" ", "%20", trim($city));

            $duplicate_content = $this->clone_content;

            $duplicate_content['title'] = str_replace("Almond", $city, $duplicate_content['title']);
            $duplicate_content['title'] = str_replace("Log Cabins", $capital_char_type, $duplicate_content['title']);


            $duplicate_content['meta_description'] = str_replace("Almond", $city, $duplicate_content['meta_description']);
            $duplicate_content['meta_description'] = str_replace("log cabins", $small_char_type, $duplicate_content['meta_description']);



            $duplicate_content['anchor_text'] = str_replace("Almond", $city, $duplicate_content['anchor_text']);
            $duplicate_content['anchor_text'] = str_replace("Log Cabins", $capital_char_type, $duplicate_content['anchor_text']);

            $duplicate_content['slug'] = str_replace("log-cabins", $type_slug, $duplicate_content['slug']);


            $duplicate_content['content'] = str_replace("Almond", $city, $duplicate_content['content']);
            $duplicate_content['content'] = str_replace("almond", $city, $duplicate_content['content']);
            
            $duplicate_content['content'] = str_replace("log cabins", $small_char_type, $duplicate_content['content']);
            $duplicate_content['content'] = str_replace("Log Cabins", $capital_char_type, $duplicate_content['content']);


            $duplicate_content['listing_header'] = str_replace("Almond", $city, $duplicate_content['listing_header']);
            $duplicate_content['listing_header'] = str_replace("Cabin", $capital_char_type, $duplicate_content['listing_header']);
           
            $duplicate_content['search_header'] = str_replace("Almond", $city, $duplicate_content['search_header']);

            $duplicate_content['sidebar'] = $sidebar[trim($city)];

            $duplicate_content['search'] = $this->createSearch($city);

            $duplicate_content['search_field_defaults'] = $this->searchFieldDefault($city);

            $duplicate_content['parent'] = $previous_city[$city];


            // $this->info($duplicate_content['search_field_defaults']);

            unset($duplicate_content['search_content']);

            try {
                $url = "https://www.smokymountainhomes4sale.com/admin/content/contentpage/add/";

                $response = $this->client->request("POST", $url, [
                    'form_params' => $duplicate_content,
                    'headers' => [
                        'Referer' => 'https://www.smokymountainhomes4sale.com/admin/content/contentpage/add/',
                        'Connection' => 'keep-alive',
                        'Host' => 'www.smokymountainhomes4sale.com',
                        'Origin' => 'https://www.smokymountainhomes4sale.com',
                        'User-Agent' => \Campo\UserAgent::random()
                    ],
                ]);

                $this->info("Success !!");
            } catch (Exception $ex) {
                print_r($ex->getMessage());
            }
        }
    }

    public function getCloneContent()
    {
        $this->info("Getting Clone Content");

        try {
            $base_url = "https://www.smokymountainhomes4sale.com/admin";
            $url = "https://www.smokymountainhomes4sale.com/admin/content/contentpage/87/change/";
            $response = $this->client->request('GET', $url);

            $response_html = (string) $response->getBody();
            $crawler = new Crawler($response_html, $base_url);

            $form = $crawler->filter('#contentpage_form')->form();
            $values = $form->getValues();

            $search = $this->search_display_option($values['search']);

            $values['search_content'] = $search;

            return $values;
        } catch (Exception $ex) {
            print_r($ex->getMessage());
        }
    }
}
