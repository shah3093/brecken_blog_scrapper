<?php

namespace App\Console\Commands;

ini_set('max_execution_time', 0);
ini_set('memory_limit', '-1');

use Goutte;
use Symfony\Component\DomCrawler\Crawler;
use Goutte\Client;

use Illuminate\Console\Command;

class scrap_rg_script_german extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'scrap_rg_script_german:leads';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Scrapes RG Leads';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->website_name = "https://www.swflhouses.com/";

        $this->output_array = [
            'id' => '',
            'option' => $this->website_name,
            'firstName' => '',
            'lastName' => '',
            'streetAddress' => '',
            'tags' => ['rg-import'],
            'leadType' => -1,
            'agent' => '',
            'assignedUser' => '',
            'listing_agent' => '',
            'lender_name' => '',
            'stage' => '',
            'opted_status' => 'Opted In',
            'emails' => array(),
            'phones' => array(),
            'type_of_properties' => array(),
            'city' => '',
            'subdivisions' => array(),
            'minimum_price' => '',
            'maximum_price' => '',
            'importantNote' => array(),
            'notes' => array(),
            'email_conversations' => array(),
            'texts' => array(),
            'phone_conversations' => array(),
            'follow_ups' => array(),
            'e_alerts' => array(),
            'listing' => array(),
            'source' => array()];

        $this->client = new Client();
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info('extracting...');
        $postData = ['email' => 'ghernandjr@gmail.com', 'password' => 'transferleads'];
        $url = 'https://login.realgeeks.com/auth/login_action';
        $this->logIn($url, $postData);
        $filename = $this->create_json("german_results_data");

        $start_page = 1;
        $end_page = 113;
        $crawler = $this->client->request('GET', 'https://leads.realgeeks.com/leads');
        for ($i = $start_page; $i <= $end_page; $i++) {
            $crawler = $this->client->request('GET', 'https://leads.realgeeks.com/leads?page=' . $i);
            $links = $crawler->filter('.leads-list__row')->each(function ( $node) {
                return $node->attr('data-url');
            });
            $this->info("******** On page ". $i . " out of ".$end_page." ********");
            $this->scrapDetailpage($links,$filename);
        };
    }

    # **************************************
    # ***************** DONE ***************

    public function logIn($url, $data) {
        $crawler = $this->client->request('POST', $url, $data);
        $this->client->request('GET', 'https://leads.realgeeks.com/login/as/realgeeks/23914');
    }

    public function scrapDetailPage($links,$filename) {
        $count_ = 0;
        foreach ($links as $link) {
            $count_+=1;
            echo $link . PHP_EOL;
            $id = explode('/', $link);
            $this->info($count_." of ".sizeof($links)." --> ".$link);

            $crawler = $this->client->request('GET', 'https://leads.realgeeks.com' . $link);


            $first_name = ($crawler->filter('#lead_first_name')->count() > 0) ? trim($crawler->filter('#lead_first_name')->attr('value')) : '';
            $last_name = ($crawler->filter('#lead_last_name')->count() > 0) ? trim($crawler->filter('#lead_last_name')->attr('value')) : '';

            /*$phone = ($crawler->filter('#lead_phone')->count() > 0) ? trim($crawler->filter('#lead_phone')->attr('value')) : '';

            $email = ($crawler->filter('#lead_email')->count() > 0) ? trim($crawler->filter('#lead_email')->attr('value')) : '';
            $work = ($crawler->filter('#lead_office_phone')->count() > 0) ? trim($crawler->filter('#lead_office_phone')->attr('value')) : '';
            $fax = ($crawler->filter('#lead_fax')->count() > 0) ? trim($crawler->filter('#lead_fax')->attr('value')) : '';

            $agent = $crawler->filter('#lead_assign_to_agent [selected=selected]')->each(function($node) {
                return trim($node->text());
            });

            $city = ($crawler->filter('#lead_city')->count() > 0) ? trim($crawler->filter('#lead_city')->attr('value')) : '';
            $status = $crawler->filter('#lead_status_id [selected=selected]')->each(function($node) {
                return trim($node->text());
            });
            $lead_urgency = $crawler->filter('#lead_urgency_id [selected=selected]')->each(function($node) {
                return trim($node->text());
            });

            $type = $crawler->filter('#lead_role_id [selected=selected]')->each(function($node) {
                return trim($node->text());
            });
            $lender = $crawler->filter('#lead_assign_to_lender [selected=selected]')->each(function($node) {
                return trim($node->text());
            });
            $streetAddress = ($crawler->filter('#lead_address')->count() > 0) ? trim((string)$crawler->filter('#lead_address')->text()) : '';
			$lead_city = ($crawler->filter('#lead_city')->count() > 0) ? trim((string)$crawler->filter('#lead_city')->attr('value')) : '';
			$lead_state = ($crawler->filter('#lead_state')->count() > 0) ? trim((string)$crawler->filter('#lead_state')->attr('value')) : '';
			$lead_postal_code = ($crawler->filter('#lead_postal_code')->count() > 0) ? trim((string)$crawler->filter('#lead_postal_code')->attr('value')) : '';

			$streetAddress = $streetAddress.'|'.$lead_city.'|'.$lead_state.'|'.$lead_postal_code;*/

            //$lastActiveDate = ($crawler->filter('.lead-header__last-active-date:nth-child(2)')->count() > 0) ? trim((string)$crawler->filter('.lead-header__last-active-date')->text()) : '';
            //$lastActiveDate = trim(ltrim($lastActiveDate,"Last Active:"));

            $this -> output_array['importantNote'] = array(
                'homeType' => ($crawler->filter('#lead_home_type [selected=selected]')->count() > 0) ? $crawler->filter('#lead_home_type [selected=selected]')->each(function($node) {
                    return trim($node->text());
                }) : '',
                'minPrice' => ($crawler->filter('#lead_minimum_price [selected=selected]')->count() > 0) ? $crawler->filter('#lead_minimum_price [selected=selected]')->each(function($node) {
                    return trim($node->text());
                }) : '',
                'minBed' => ($crawler->filter('#lead_minimum_bedrooms [selected=selected]')->count() > 0) ? $crawler->filter('#lead_minimum_bedrooms [selected=selected]')->each(function($node) {
                    return trim($node->text());
                }) : '',
                'maxPrice' => ($crawler->filter('#lead_maximum_price [selected=selected]')->count() > 0) ? $crawler->filter('#lead_maximum_price [selected=selected]')->each(function($node) {
                    return trim($node->text());
                }) : '',
                'minBaths' => ($crawler->filter('#lead_minimum_bathrooms [selected=selected]')->count() > 0) ? $crawler->filter('#lead_minimum_bathrooms [selected=selected]')->each(function($node) {
                    return trim($node->text());
                }) : '',
                'sqrft' => ($crawler->filter('#lead_square_footage [selected=selected]')->count() > 0) ? $crawler->filter('#lead_square_footage [selected=selected]')->each(function($node) {
                    return trim($node->text());
                }) : '',
                'note' => ($crawler->filter('#lead_notes')->count() > 0) ? trim((string)$crawler->filter('#lead_notes')->text()) : ''
            );

            $name = $first_name . ' ' . $last_name;
            $this->info($name);

            $this->output_array['id'] = $id[sizeof($id) - 1];
            //$this->output_array['streetAddress'] = $streetAddress;
            /*$this -> check_opted_status($this->output_array['id']);

            $this->output_array['assignedUser'] = (isset($agent[0]) ? $agent[0] : '');
            $this->output_array['lender_name'] = (isset($lender[0])) ? $lender[0] : '';*/
            $this->output_array['firstName'] = $first_name;
            $this->output_array['lastName'] = $last_name;
            /*$this->output_array['streetAddress'] = $streetAddress;
            $this->output_array['city'] = (isset($city)) ? $city : '';
            $this->output_array['stage'] = (isset($status[0])) ? $status[0] : '';
            $this->output_array['lead_urgency'] = (isset($lead_urgency[0])) ? $lead_urgency[0] : '';
            $this->output_array['lastActiveDate'] = $lastActiveDate;

            $this->output_array['emails']=[];
            array_push($this->output_array['emails'], ((isset($email)) ? $email : ''));

            $this->output_array['phones']=[];
            if (isset($phone)){
                array_push($this->output_array['phones'], $phone);
            }
            if ($work != ""){
                array_push($this->output_array['phones'], $work);
            }
            if ($fax != ""){
                array_push($this->output_array['phones'], $fax);
            }

            if (count($type) != 0){
                $this->output_array['leadTypeText'] = $type[0];
                if ($type[0] == "Buyer"){
                    $this->output_array['leadType'] = 2;
                }
                elseif ($type[0] == "Seller") {
                    $this->output_array['leadType'] = 1;
                }
                elseif($type[0] == "Buyer and seller"){
                    $this->output_array['leadType'] = 3;
                }else{
                    $this->output_array['leadType'] = -1;
                }
            }
            else{
                $this->output_array['leadType'] = -1;
                $this->output_array['leadTypeText'] = "";
            }
            $tagsStr = str_replace(", ",",",$crawler->filter('#lead_tag_list')->attr('value'));
            if(!empty($tagsStr)){
                $tags_tmp = explode(',',$tagsStr);
                $tags_tmp[] = 'rg-import';
            }else{
                $tags_tmp = array('rg-import');
            };
            $this->output_array['tags'] = $tags_tmp;

            $c=1;
            $sources =array();
            ($crawler->filter('.additional-details__group3 .additional-details__column span'))->each(function($source) use (&$c, &$sources){
                if($c==1){
                    $sources['source']=$source->text();
                }else{
                    $sources['detail']=$source->text();
                }
                $c= $c+1;
            });


            $this->output_array['source'] = $sources;
            $this->crawlNotes($id[sizeof($id) - 1]);
            $this->crawlEmails($id[sizeof($id) - 1]);
            $this->crawlTexts($id[sizeof($id) - 1]);
            $this->crawlPhones($id[sizeof($id) - 1]);
            $this->crawlLisitng($id[sizeof($id) - 1]);
            $this->crawlFollowUps($id[sizeof($id) - 1]);
            $this->crawlProperties();
            $this->crawlSearches($id[(sizeof($id) - 1)]);*/


            
            $JSONPData = json_encode($this->output_array, JSON_PRETTY_PRINT);
            if($JSONPData!= false){
                $this->saveJsonToFile(json_encode($this->output_array, JSON_PRETTY_PRINT),$filename);
            }else{
                $this->output_array['fav_properties'] = array();
                $this->saveJsonToFile(json_encode($this->output_array, JSON_PRETTY_PRINT),$filename);

            };
            $this->output_array = [
                'id' => '',
                'option' => $this->website_name,
                'firstName' => '',
                'lastName' => '',
                'streetAddress' => '',
                'tags' => array(),
                'leadType' => -1,
                'agent' => '',
                'assignedUser' => '',
                'listing_agent' => '',
                'lender_name' => '',
                'stage' => '',
                'opted_status' => 'Opted In',
                'emails' => array(),
                'phones' => array(),
                'type_of_properties' => array(),
                'city' => '',
                'subdivisions' => array(),
                'minimum_price' => '',
                'maximum_price' => '',
                'importantNote' => array(),
                'notes' => array(),
                'email_conversations' => array(),
                'texts' => array(),
                'phone_conversations' => array(),
                'follow_ups' => array(),
                'e_alerts' => array(),
                'listing' => array()];
                
        }
    }

    public function crawlNotes($id) {
        $mainNotes = [];
        $crawler = $this->client->request('GET', 'https://leads.realgeeks.com/leads/' . $id . '/lead_activities/search?search%5Baction_type%5D=Notes');

        $html = json_decode($this->client->getResponse()->getContent(), TRUE);

        $notesHtml = new Crawler($html['activities']);
        $notes = $this->scrapeActivity($notesHtml);

        $mainNotes = array_merge($mainNotes, $notes);

        $this->output_array['notes'] = $mainNotes;
    }

    public function crawlEmails($id){
        $mainEmails = [];
        $crawler = $this->client->request('GET', 'https://leads.realgeeks.com/leads/' . $id . '/lead_activities/search?search%5Baction_type%5D=Email');
        $html = json_decode($this->client->getResponse()->getContent(), TRUE);

        $emailsHtml = new Crawler($html['activities']);
        $emails = $this->scrapeActivity($emailsHtml);

        $emailsHtml = array_merge($mainEmails, $emails);

        $this->output_array['email_conversations'] = $emailsHtml;
    }

    public function crawlTexts($id){
        $mainTexts = [];
        $crawler = $this->client->request('GET', 'https://leads.realgeeks.com/leads/'.$id.'/lead_activities/search?search%5Baction_type%5D=SMS');
        $html = json_decode($this->client->getResponse()->getContent(), TRUE);

        $textsHtml = new Crawler($html['activities']);
        $texts = $this->scrapeActivity($textsHtml);

        $mainTexts = array_merge($mainTexts, $texts);

        $this->output_array['texts'] = $mainTexts;
    }

    public function crawlPhones($id){
        $mainPhones = [];
        $crawler = $this->client->request('GET', 'https://leads.realgeeks.com/leads/' . $id . '/lead_activities/search?search%5Baction_type%5D=Phone');
        $html = json_decode($this->client->getResponse()->getContent(), TRUE);

        $phonesHtml = new Crawler($html['activities']);
        $phones = $this->scrapeActivity($phonesHtml);

        $mainPhones = array_merge($mainPhones, $phones);

        $this->output_array['phone_conversations'] = $mainPhones;
    }

    public function check_opted_status($id){
        $crawler = $this->client->request('GET', 'https://leads.realgeeks.com/leads/' . $id . '/lead_activities/search?search%5Baction_type%5D=Misc');
        $html = json_decode($this->client->getResponse()->getContent(), TRUE);

        $miscHTML = new Crawler($html['activities']);

        $status = ($miscHTML->filter('.user-delete')->count() > 0) ? trim($miscHTML->filter('.user-delete')->html()) : '';
        $this->info($status);
        if($status == "Opted Out"){
            $this->output_array['opted_status'] = $status;
            $opted_out_note = array(
                'date' => '',
                'content' => "Opted-Out"
                );
            array_push($this->output_array['notes'],$opted_out_note);
            $this->info("RETURNING 0");
            return 0;
        }
        elseif($status == "Deleted From Website"){
            $this->output_array['opted_status'] = $status;
            $opted_out_note = array(
                'date' => '',
                'content' => "Deleted From Website"
                );
            array_push($this->output_array['notes'],$opted_out_note);
            return 0;
        }
        return 1;
    }

    public function scrapeActivity($notesHtml) {

        $emails = [];
        $count=0;
        if ($notesHtml->filter('.activity-row')->count() > 0) {
            $notesHtml->filter('.activity-row')->each(function($node) use (&$emails, &$count) {
                // ************ date *************
                $emails[$count]['date'] = ($node->filter('.lead-activities__datetime')->count() > 0) ? trim($node->filter('.lead-activities__datetime')->text()) : '';
                $emails[$count]['action'] = ($node->filter('.lead-activities__action')->count() > 0) ? trim(strip_tags($node->filter('.lead-activities__action')->text())) : '';

                // ************ description *************
                $temp_description = ($node->filter('.lead-activities__content.lm-table__column')->count() > 0) ? trim($node->filter('.lead-activities__content.lm-table__column')->html()) : '';

                $temp_description = str_replace("<strong>","",$temp_description);
                $temp_description = str_replace("</strong>","",$temp_description);
                $temp_description = str_replace("<br>","",$temp_description);
                $temp_description = str_replace("</a>","",$temp_description);
                $temp_description = str_replace("<p>","",$temp_description);
                $temp_description = str_replace("</p>","",$temp_description);
                $temp_description = str_replace("Reply","",$temp_description);

                $temp_description = str_replace("<div>","",$temp_description);
                $temp_description = str_replace("</div>","",$temp_description);
                $temp_description = str_replace("<img>","",$temp_description);
                $temp_description = str_replace("<span>","",$temp_description);
                $temp_description = str_replace("</span>","",$temp_description);
                $temp_description = str_replace("\n\n","\n",$temp_description);
                $temp_description = str_replace("\n\n","\n",$temp_description);

                $pattern ='/<a[^>]*([^<])/'; #regex for anchor tags
                $temp_description=preg_replace($pattern, "", $temp_description);


                if (strlen($temp_description)>0){
                    $emails[$count]['content'] = trim($temp_description);
                }
                else{
                    $emails[$count]['content'] = "";
                }
                $count+=1;
            });
        }
        return $emails;
    }

    public function crawlSearches($id) {
        $searches = $this->crawlSavedSearches(($this->output_array['emails'])[0], ($this->output_array['phones'])[0]);
        if (sizeof($searches)>0){
            $this->output_array['saved_searches'] = $searches;
        }
    }

    public function crawlProperties() {
    if(count($this->output_array['emails']) < 1){
        return;
    };
    if(count($this->output_array['phones']) < 1){
        return;
    };

        $fav_properties = $this->crawlFavProperties($this->output_array['emails'][0],$this->output_array['phones'][0]);
        if (sizeof($fav_properties)>0){
            $this->output_array['fav_properties'] = $fav_properties;
        }
    }
    public function crawlFavProperties($username, $phone) {
        $property = [];
        $this->username = $username;
        $this->phone = $phone;
        $crawler = $this->property_login($username, $phone);
        $property = $this->scrapFavProperties($crawler);
        return $property;
    }
    public function scrapFavProperties($crawl){
        $properties = array();
        if ($crawl->filter('[class="row property results"]')->count() > 0) {
            $crawl->filter('[class="row property results"]')->each(function($node) use (&$properties) {
             $new_property = array('address'=>'','building_name'=>'','url'=>'','price'=>'');
             $new_property['address'] = ($node->filter('.address')->count() > 0) ? trim($node->filter('.address')->text()) : '';
             $new_property['building_name'] = ($node->filter('.address .building_name')->count() > 0) ? trim($node->filter('.address .building_name')->text()) : '';
             $node->filter('[class="small-12 medium-7 columns property-details featured-details"]')->each(function($detail)  use (&$new_property){
                $detail->filter('.detail')->each(function($link) use (&$new_property){
                $new_property[strtolower($link->filter('.detail-title')->text())] = trim($link->filter('.number')->text());
                });
             });
             $website = rtrim($this->website_name,"/");
             $new_property['url'] = ($node->filter('.price')->count() > 0) ? $website.trim($node->filter('.price')->attr('href')) : '';
             $new_property['price'] = ($node->filter('.price')->count() > 0) ? trim($node->filter('.price')->text()) : '';
             $url_crawl = $this->sendGetRequest($new_property['url']);
             $url_crawl->filter('.property_detail_specs')->each(function($specs) use (&$new_property){
                if(trim($specs->filter('dt')->text()) == 'Type') {
                    $new_property['property_type'] = $specs->filter('dd')->text();
                }
             });
            array_push($properties,$new_property);
            });
        }

        if ($crawl->filter('[class="row property results property-sold sold-price-down"]')->count() > 0) {
            $crawl->filter('[class="row property results property-sold sold-price-down"]')->each(function($node) use (&$properties) {
             $new_property = array('address'=>'','building_name'=>'','url'=>'','price'=>'');
             $new_property['address'] = ($node->filter('.address')->count() > 0) ? trim($node->filter('.address')->text()) : '';
             $new_property['building_name'] = ($node->filter('.address .building_name')->count() > 0) ? trim($node->filter('.address .building_name')->text()) : '';
             $node->filter('[class="small-12 medium-7 columns property-details featured-details"]')->each(function($detail)  use (&$new_property){
                $detail->filter('.detail')->each(function($link) use (&$new_property){
                $new_property[strtolower($link->filter('.detail-title')->text())] = trim($link->filter('.number')->text());
                });
             });
             $website = rtrim($this->website_name,"/");
             $new_property['url'] = ($node->filter('.price')->count() > 0) ? $website.trim($node->filter('.price')->attr('href')) : '';
             $new_property['price'] = ($node->filter('.price')->count() > 0) ? trim($node->filter('.price')->text()) : '';
             $url_crawl = $this->sendGetRequest($new_property['url']);
             $url_crawl->filter('.property_detail_specs')->each(function($specs) use (&$new_property){
                if(trim($specs->filter('dt')->text()) == 'Type') {
                    $new_property['property_type'] = $specs->filter('dd')->text();
                }
             });
            array_push($properties,$new_property);
            });
        }

        if ($crawl->filter('[class="row property results property-sold sold-price-up"]')->count() > 0) {
            $crawl->filter('[class="row property results property-sold sold-price-up"]')->each(function($node) use (&$properties) {
             $new_property = array('address'=>'','building_name'=>'','url'=>'','price'=>'');
             $new_property['address'] = ($node->filter('.address')->count() > 0) ? trim($node->filter('.address')->text()) : '';
             $new_property['building_name'] = ($node->filter('.address .building_name')->count() > 0) ? trim($node->filter('.address .building_name')->text()) : '';
             $node->filter('[class="small-12 medium-7 columns property-details featured-details"]')->each(function($detail)  use (&$new_property){
                $detail->filter('.detail')->each(function($link) use (&$new_property){
                $new_property[strtolower($link->filter('.detail-title')->text())] = trim($link->filter('.number')->text());
                });
             });
             $website = rtrim($this->website_name,"/");
             $new_property['url'] = ($node->filter('.price')->count() > 0) ? $website.trim($node->filter('.price')->attr('href')) : '';
             $new_property['price'] = ($node->filter('.price')->count() > 0) ? trim($node->filter('.price')->text()) : '';
             $url_crawl = $this->sendGetRequest($new_property['url']);
             $url_crawl->filter('.property_detail_specs')->each(function($specs) use (&$new_property){
                if(trim($specs->filter('dt')->text()) == 'Type') {
                    $new_property['property_type'] = $specs->filter('dd')->text();
                }
             });
            array_push($properties,$new_property);
            });
        }
        return $properties;
    }
    public function property_login($username, $phone) {
        $params = ['email' => trim($this->username), 'phone_number' => trim($this->phone)];
        $url = $this->website_name;
        $crawler = $this->sendPropertyRequest($url."/member/login/", $params, $url);
        return $crawler;
    }

    public function crawlSavedSearches($username, $phone) { //echo "hello2";
        $searches = [];
        $this->username = $username;
        $this->phone = $phone;
        //var_dump($username."---------------------------".$phone);
        $crawler = $this->search_login($username, $phone);
        $searches = $this->scrapSearches($crawler);
        return $searches;
    }

    public function search_login($username, $phone) {

        $params = ['email' => trim($this->username), 'phone_number' => trim($this->phone)];
        //var_dump($params);
        $url = $this->website_name;
        $crawler = $this->sendPostRequest($url."/member/login/", $params, $url);
        return $crawler;
    }

    public function sendPostRequest($url, $params, $baseUrl){
        $this->sendGetRequest($baseUrl . '/member/ajax/csrf/');
        $headers = $this->client->getResponse()->getHeaders();

        $split_header = explode(' ', $headers['Set-Cookie'][0]);
        $csrf_array = explode('=', $split_header[0]);
        $csrf = preg_replace('/;/', '', $csrf_array[1]);
        $this->client->setHeader('X-CSRFToken', $csrf);
        $this->client->request('POST', $url, $params);

        $crawler = $this->sendGetRequest($baseUrl . '/member/saved_searches/');

        return $crawler;
    }

    public function sendPropertyRequest($url, $params, $baseUrl){
        $this->sendGetRequest($baseUrl . '/member/ajax/csrf/');
        $headers = $this->client->getResponse()->getHeaders();

        $split_header = explode(' ', $headers['Set-Cookie'][0]);
        $csrf_array = explode('=', $split_header[0]);
        $csrf = preg_replace('/;/', '', $csrf_array[1]);
        $this->client->setHeader('X-CSRFToken', $csrf);
        $this->client->request('POST', $url, $params);

        $crawler = $this->sendGetRequest($baseUrl . '/member/favorite_properties/');

        return $crawler;
    }

    public function sendGetRequest($url){
        $crawler = $this->client->request('GET', $url);
        return $crawler;
    }

    public function scrapSearches($crawler) { //echo "hello3";


        $inquiry = [];
        $alerts =[];
        $locations =[];
        $market_reports = [];
        $completeSearchCriteria = [];
        if ($crawler->filter('[class="row saved-search collapse"]')->count() > 0) {
            $crawler->filter('[class="row saved-search collapse"]')->each(function($node) use (&$inquiry,&$alerts,&$locations,&$market_reports,&$completeSearchCriteria) {

                $propertyType = [];
                $zip = '';
                $minPrice = -1;
                $maxPrice = -1;
                $minBed = -1;
                $minBath = -1;
                $sizeMax = -1;
                $sizeMin = -1;

                $rawString = trim($node->filter('[class="s-search-details medium-6 columns"] > p')->text());
                $frequencyText = trim($node->filter('[selected="selected"]')->text());
                $frequencyValue = 1;
                if($frequencyText=='Daily'){
                    $frequencyValue = 1;
                }elseif ($frequencyText=='Weekly') {
                    $frequencyValue = 7;
                }elseif ($frequencyText=='Bi-Weekly') {
                    $frequencyValue = 14;

                }elseif ($frequencyText=='Monthly') {
                    $frequencyValue = 28;

                }elseif ($frequencyText=='Never') {
                    $frequencyValue = 2;

                }


                preg_match("/Single Family Residential/i", $rawString, $output_array);
                if (isset($output_array[0])){
                    array_push($propertyType, "Single Family Home");
                }

                preg_match("/Land/i", $rawString, $output_array);
                if (isset($output_array[0])){
                    array_push($propertyType, "Vacant Land");
                }

                preg_match("/Multifamily/i", $rawString, $output_array);
                if (isset($output_array[0])){
                    array_push($propertyType, "Multi-Family");
                }


                preg_match("/Condo/i", $rawString, $output_array);
                if (isset($output_array[0])){
                    array_push($propertyType, "Condo");
                }

                preg_match("/Townhome/i", $rawString, $output_array);
                if (isset($output_array[0])){
                    array_push($propertyType, "Townhome");
                }

                $link = ($node->filter('div.s-search-details.medium-6.columns > a')->count() > 0) ? ($node->filter('div.s-search-details.medium-6.columns > a')->attr('href')) : '';
                var_dump($link);
                if ($link === '#') {
                    // extract price via regex
                    preg_match("/\\\$(.*[0-9+])/", $rawString, $matches);
                    if (isset($matches[0])) {

                        $split_price = explode('and', $matches[0]);

                        if (sizeof($split_price) > 1) {

                            $minPrice = trim($split_price[0]);
                            $maxPrice = trim($split_price[1]);
                        }
                    }
                    preg_match("/city of (\w+)/", $rawString, $output_array);

                    if (isset($output_array[1])) {
                        array_push($city, $output_array[1]);
                    }
                }else{

                    $link_text = ($node->filter('div.s-search-details.medium-6.columns > a')->count() > 0) ? ($node->filter('div.s-search-details.medium-6.columns > a')->text()) : '';
                    if(strpos($link, '/marketreport/') !== false){
                        $report_link =  $node->filter('div.s-options.medium-2.columns > a')->each(function($linke){
                            if(strpos($linke->attr('href'),'edit')){
                                return $linke->attr('href');
                            }
                        });
                        $frequency_value= $frequencyValue;
                        $report_link = $report_link[1];
                        if($report_link){
                            $edit_crawler = $this->sendGetRequest($this->website_name . $report_link);
                            $market_analysis = array('report_name'=>trim($link_text),'frequency_value'=>$frequencyValue);
                            $market_analysis['location'] = array();

                            $county_data = $edit_crawler->filter('#id_county [selected="selected"]')->each(function($option){
                                return trim($option->text());
                            });
                            $region_data = $edit_crawler->filter('#id_region [selected="selected"]')->each(function($option){
                                return trim($option->text());
                            });
                            $market_analysis['location']['county'] = array();
                            $market_analysis['location']['county'] = $county_data;
                            /*$market_analysis['location']['county'] = ($edit_crawler->filter('#id_county [selected="selected"]')->count() > 0) ? trim($edit_crawler->filter('#id_county [selected="selected"]')->text()) : '';*/
                            $city_data = $edit_crawler->filter('#id_city [selected="selected"]')->each(function($option){
                                return trim($option->text());
                            });
                            $market_analysis['location']['city'] = array();
                            $market_analysis['location']['city'] = $city_data;
                            /*$market_analysis['location']['city'] = ($edit_crawler->filter('#id_city [selected="selected"]')->count() > 0) ? trim($edit_crawler->filter('#id_city [selected="selected"]')->text()) : '';*/
                            $subdivision_data = $edit_crawler->filter('#id_subdivision [selected="selected"]')->each(function($option){
                                return trim($option->text());
                            });
                            $market_analysis['location']['subdivision'] = array();
                            $market_analysis['location']['subdivision'] = $subdivision_data;
                            /*$market_analysis['location']['subdivision'] = ($edit_crawler->filter('#id_subdivision [selected="selected"]')->count() > 0) ? trim($edit_crawler->filter('#id_subdivision [selected="selected"]')->text()) : '';*/
                            $subsection_data = $edit_crawler->filter('#id_tract [selected="selected"]')->each(function($option){
                                return trim($option->text());
                            });
                            $market_analysis['location']['subsection'] = array();
                            $market_analysis['location']['subsection'] = $subsection_data;

                            /*$market_analysis['location']['subsection'] = ($edit_crawler->filter('#id_tract [selected="selected"]')->count() > 0) ? trim($edit_crawler->filter('#id_tract [selected="selected"]')->text()) : '';*/
                            $market_analysis['property_details'] = array();
                            $property_type =  $edit_crawler->filter('.property-type label')->each(function($linke){
                                if(!empty($linke->filter('input')->attr('checked'))){
                                    return trim($linke->text());
                                }
                            });
                            $market_analysis['property_details']['property_type'] = array();
                            $market_analysis['property_details']['property_type'] = array_filter($property_type, function($value) {
                                $value = trim($value);
                                return !is_null($value) && $value !== ''; });

                            $market_analysis['property_details']['min_price'] = ($edit_crawler->filter('#id_list_price_min [selected="selected"]')->count() > 0) ? trim($edit_crawler->filter('#id_list_price_min [selected="selected"]')->text()) : '';

                            $market_analysis['property_details']['max_price'] = ($edit_crawler->filter('#id_list_price_max [selected="selected"]')->count() > 0) ? trim($edit_crawler->filter('#id_list_price_max [selected="selected"]')->text()) : '';

                            $market_analysis['property_details']['sold_within'] = ($edit_crawler->filter('#id_sold_within [selected="selected"]')->count() > 0) ? trim($edit_crawler->filter('#id_sold_within [selected="selected"]')->text()) : '';

                            $market_analysis['property_details']['beds_min'] = ($edit_crawler->filter('#id_beds_min [selected="selected"]')->count() > 0) ? trim($edit_crawler->filter('#id_beds_min [selected="selected"]')->text()) : '';

                            $market_analysis['property_details']['baths_min'] = ($edit_crawler->filter('#id_baths_min [selected="selected"]')->count() > 0) ? trim($edit_crawler->filter('#id_baths_min [selected="selected"]')->text()) : '';

                            $market_analysis['property_details']['year_built'] = ($edit_crawler->filter('#id_year_built_min [selected="selected"]')->count() > 0) ? trim($edit_crawler->filter('#id_year_built_min [selected="selected"]')->text()) : '';

                            $market_analysis['property_details']['area'] = ($edit_crawler->filter('#id_area_min [selected="selected"]')->count() > 0) ? trim($edit_crawler->filter('#id_area_min [selected="selected"]')->text()) : '';

                            $market_analysis['property_details']['acres'] = ($edit_crawler->filter('#id_acres [selected="selected"]')->count() > 0) ? trim($edit_crawler->filter('#id_acres [selected="selected"]')->text()) : '';

                            $market_analysis['property_details']['short_sale'] = ($edit_crawler->filter('#id_short_sale [selected="selected"]')->count() > 0) ? trim($edit_crawler->filter('#id_short_sale [selected="selected"]')->text()) : '';

                            $lot_description_data = $edit_crawler->filter('#id_lot_description [selected="selected"]')->each(function($option){
                                return trim($option->text());
                            });
                            $market_analysis['property_details']['lot_description'] = array();
                            $market_analysis['property_details']['lot_description'] = $lot_description_data;
                            /*
                            $market_analysis['property_details']['lot_description'] = ($edit_crawler->filter('#id_lot_description [selected="selected"]')->count() > 0) ? trim($edit_crawler->filter('#id_lot_description [selected="selected"]')->text()) : '';*/
                            $interior_features_data = $edit_crawler->filter('#id_interior_features [selected="selected"]')->each(function($option){
                                return trim($option->text());
                            });
                            $market_analysis['property_details']['interior_features'] = array();
                            $market_analysis['property_details']['interior_features'] = $interior_features_data;
                            /*
                            $market_analysis['property_details']['interior_features'] = ($edit_crawler->filter('#id_interior_features [selected="selected"]')->count() > 0) ? trim($edit_crawler->filter('#id_interior_features [selected="selected"]')->text()) : '';*/
                            $exterior_features_data = $edit_crawler->filter('#id_exterior_features [selected="selected"]')->each(function($option){
                                return trim($option->text());
                            });
                            $market_analysis['property_details']['exterior_features'] = array();
                            $market_analysis['property_details']['exterior_features'] = $exterior_features_data;
                            /*
                            $market_analysis['property_details']['exterior_features'] = ($edit_crawler->filter('#id_exterior_features [selected="selected"]')->count() > 0) ? trim($edit_crawler->filter('#id_exterior_features [selected="selected"]')->text()) : '';*/
                            array_push($market_reports,$market_analysis);
                        }
                    }else{
                        //echo $link;
                         $edit_crawler = $this->sendGetRequest($this->website_name . $link);
                         $details_link = ($edit_crawler->filter('div.miranda-wrapper > div > dl > dd:nth-child(2) > a')->count() > 0) ? trim($edit_crawler->filter('div.miranda-wrapper > div > dl > dd:nth-child(2) > a')->attr('href')) : '';
                        //$this->info($details_link);
                        if ($details_link!=''){
                            //$temp = explode("/",$details_link);
                            //$temp = $temp[sizeof($temp) - 1];
                            //$temp = str_replace("?","",$temp);
                            $temp = explode("?",$details_link); //by saba
                            $temp = $temp[1];
                            $temp = str_replace("%20"," ",$temp);
                            $temp = explode("&", $temp);
                            #$this->print_array($temp);
                            $city_temp=[];
                            $alertsArray= array();
                            $completeDataArray = array();
                            foreach ($temp as $data) {
                                //$this->info($data);
                                if (strlen($data) == 0){
                                    continue;
                                }
                                $data = explode("=", $data);
                                //$this->info($data[0]);
                                if(!isset($data[1])){
                                    continue;
                                }
                                if($data[0]==""){
                                    $data[0]=="MZip";
                                    $zip=$data[1];
                                }
                                $completeDataArray['search_title'] = trim($link_text);
                                $city_temp['search_title'] = trim($link_text);
                                $alertsArray['search_title'] = trim($link_text);
                                if(isset($completeDataArray[$data[0]]) && !empty($completeDataArray[$data[0]])){

                                    $completeDataArray[$data[0]]= $completeDataArray[$data[0]].";-;".$data[1];

                                }else{
                                    $completeDataArray[$data[0]]= $data[1];
                                }

                                if($data[0]=="zip"){
                                    $alertsArray['zip'][] = $data[1];
                                    $city_temp['type']='zipCode';
                                    $city_temp['zipValue'][]=$data[1];
                                    $city_temp['stateCode']="";
                                }elseif(strlen($zip)>0){
                                    $alertsArray['zip'][]=$zip;
                                    $city_temp['type']='zipCode';
                                    $city_temp['zipValue'][]=$zip;
                                    $city_temp['stateCode']="";
                                    }
                                if($data[0]=="county"){
                                    $alertsArray['county'][] = $data[1];
                                    $city_temp['type']='county';
                                    $city_temp['countyValue'][]=$data[1];
                                    $city_temp['stateCode']="";
                                }
                                if($data[0]=="city"){
                                    $city_temp['type']='city';
                                    $city_temp['cityValue'][]=$data[1];
                                    $city_temp['stateCode']= "";
                                    $alertsArray['city'][]=$data[1];
                                }

                               if($data[0]=="neighborhood"){
                                    $alertsArray['neighborhood'][]=$data[1];
                                    //$city_temp['city']=$data[1];
                               }
                               if($data[0]=="subdivision"){
                                    $alertsArray['subdivision'][]=$data[1];
                                    //$city_temp['city']=$data[1];
                               }
                               if($data[0]=="search_status"){
                                    $alertsArray['search_status']=$data[1];
                                    //$city_temp['city']=$data[1];
                               }

                                if($data[0]=="list_price_max"){
                                    $maxPrice = (int)$data[1];
                                    $city_temp['maxPrice'] = $maxPrice;
                                    $alertsArray['maxPrice']=$maxPrice;
                                }
                                if($data[0]=="list_price_min"){
                                    $minPrice = (int)$data[1];
                                    $city_temp['minPrice'] = $minPrice;
                                    $alertsArray['minPrice']=$minPrice;
                                }
                                if($data[0]=="baths_min"){
                                    $minBath = (int)$data[1];
                                    $city_temp['minBath']=$minBath;
                                    $alertsArray['minBath']=$minBath;
                                }
                                if($data[0]=="beds_min"){
                                    $minBed = (int)$data[1];
                                    $city_temp['minBed']=$minBed;
                                    $alertsArray['minBed']=$minBed;
                                }
                                if($data[0]=="minSqFt"){
                                    $sizeMax = (int)$data[1];
                                    $city_temp['sizeMax']=$sizeMax;
                                    $alertsArray['sizeMax']=$sizeMax;
                                }
                                if($data[0]=="maxSqft"){
                                    $sizeMin = (int)$data[1];
                                    $city_temp['sizeMin']=$sizeMin;
                                    $alertsArray['sizeMin']=$sizeMin;
                                }
                            }

                            if($minPrice>0 and $maxPrice==0){
                                $maxPrice = 10000000;
                                $city_temp['maxPrice']=$maxPrice;
                                $alertsArray['maxPrice'] = $maxPrice;
                            }
                            //add property type
                            $city_temp['propertyType']=$propertyType;
                            $alertsArray['propertyType']=$propertyType;
                            $alertsArray['frequency'] = $frequencyValue;
                            //var_dump($alertsArray) ;
                            //if (strpos($details_link, 'city') !== false || strpos($details_link, 'county') !== false || strpos($details_link, 'zip') !== false ||(strlen($zip)>0) ) {
                                array_push($locations, $city_temp); //echo "location pushed";
                            //}
                            array_push($alerts, $alertsArray);
                            array_push($completeSearchCriteria, $completeDataArray);
                        }
                    }


                }
                $inquiry = [
                    'market_reports'=>$market_reports,
                    'inquiries' => $locations,
                    'alerts'=>$alerts,
                    'completeSearchCriteria'=>$completeSearchCriteria,
                ];
                });
                #$this->output_array['email_conversations'] = $emailsHtml;
        }
        $this->sendGetRequest($this->website_name.'/member/logout/');
        return $inquiry;
    }

    public function print_array($arr){
        $this->info("******* ARRAY ***********");
        foreach ($arr as $data){
            $this->info($data);
        }
        $this->info("******* END ***********");
    }

    public function create_json($fileName){
        $filename = $fileName."_".time().".json";
        $handle = @fopen(storage_path('german/'.$filename), 'w+');
        return $filename;
    }

    public function saveJsonToFile($data,$filename) {


            // read the file if present

            $handle = @fopen(storage_path('german/'.$filename), 'r+');


            if ($handle) {
            // seek to the end
                fseek($handle, 0, SEEK_END);

            // are we at the end of is the file empty
                if (ftell($handle) > 0) {
            // move back a byte
                    fseek($handle, -1, SEEK_END);
            // add the trailing comma
                    fwrite($handle, ',', 1);

            // add the new json string
                    fwrite($handle, $data . ']');
                } else {
            // write the first event inside an array
                    fwrite($handle, '[' . $data . ']');
                }

            // close the handle on the file
                fclose($handle);
            }
    }

    public function crawlLisitng($id) {
        $viewedListingMain = [];
        $savedListingMain = [];

        $count = 0;
        $crawler = $this->client->request('GET', 'https://leads.realgeeks.com/leads/' . $id . '/lead_activities/search?search%5Baction_type%5D=Properties');
        $html = json_decode($this->client->getResponse()->getContent(), TRUE);
        $listingHtml = new Crawler($html['activities']);
        $data = $this->scrapListing($listingHtml);

        #$viewedListingMain = array_merge($viewedListingMain, $data['viewed']);
        $savedListingMain = array_merge($savedListingMain, $data['saved']);
        #$this->output_array['listing']['viewed'] = $viewedListingMain;
        $this->output_array['listing']['saved'] = $savedListingMain;
    }

    public function scrapListing($listingHtml) {
        $viewedListing = [];
        $savedListing = [];
        $count = 0;
        $flag=1;
        if ($listingHtml->filter('.activity-row')->count() > 0) {
            $listingHtml->filter('.activity-row')->each(function($node) use (&$viewedListing, &$savedListing, &$count, $flag) {

                $check = trim($node->filter('.lead-activities__action > span')->text());
                if ($check=='Favorite Property Removed'){
                    $flag=0;
                }
                elseif ($check == 'Property Viewed' and $flag){
                    $flag=1;
                    $buyer = ($node->filter('.lead-activities__initiator > strong')->count() > 0) ? trim($node->filter('.lead-activities__initiator > strong')->text()) : '';
                    $listing_date = ($node->filter('.lead-activities__datetime')->count() > 0) ? trim($node->filter('.lead-activities__datetime')->text()) : '';
                    $url = ($node->filter('[class="lead-activities__content lm-table__column"] > p > a')->count() > 0) ? trim($node->filter('[class="lead-activities__content lm-table__column"] > p > a')->text()) : '';

                    $data = ($node->filter('[class="lead-activities__content lm-table__column"]')->count() > 0) ? trim($node->filter('[class="lead-activities__content lm-table__column"]')->text()) : '';
                    $split_data = explode('-', $data);

                    if (isset($split_data[0])) {
                        $mls = $split_data[0];
                    } else {
                        $mls = '';
                    }
                    if (isset($split_data[1])) {
                        $price = $split_data[1];
                    } else {
                        $price = '';
                    }
                    if (isset($split_data[2])) {
                        $property = $split_data[2];
                    } else {
                        $property = '';
                    }

                    if (isset($split_data[3])) {
                        $lines = preg_split('/\r\n|\r|\n/', $split_data[3]);
                        $property_type = (isset($lines[0])) ? $lines[0] : '';
                    } else {
                        $property_type = '';
                    }


                    $viewedListing[] = ['date' => $listing_date, 'buyer' => $buyer, 'url' => $url, 'mls' => $mls, 'price' => $price, 'property' => $property, 'property_type' => $property_type];
                } else {
                    // saved listing
                    $buyer = ($node->filter('.lead-activities__initiator > strong')->count() > 0) ? trim($node->filter('.lead-activities__initiator > strong')->text()) : '';
                    $listing_date = ($node->filter('.lead-activities__datetime')->count() > 0) ? trim($node->filter('.lead-activities__datetime')->text()) : '';

                    $saved_data = ($node->filter('[class="lead-activities__content lm-table__column"]')->count() > 0) ? trim($node->filter('[class="lead-activities__content lm-table__column"]')->text()) : '';

                    preg_match("/MLS#:(.*)/", $saved_data, $matches);
                    if (isset($matches[1])) {
                        $mls = $matches[1];
                    } else {
                        $mls = '';
                    }
                    preg_match("/Property:(.*)/", $saved_data, $matches);
                    if (isset($matches[1])) {
                        $property = $matches[1];
                    } else {
                        $property = '';
                    }
                    preg_match("/Property Type:(.*)/", $saved_data, $matches);
                    if (isset($matches[1])) {
                        $property_type = $matches[1];
                    } else {
                        $property_type = '';
                    }
                    preg_match("/Listing Price:(.*)/", $saved_data, $matches);
                    if (isset($matches[1])) {
                        $price = $matches[1];
                    } else {
                        $price = '';
                    }
                    preg_match("/Url:(.*)/", $saved_data, $matches);
                    if (isset($matches[1])) {
                        $url = $matches[1];
                    } else {
                        $url = '';
                    }

                    $savedListing[] = ['date' => $listing_date, 'buyer' => $buyer, 'url' => $url, 'mls' => $mls, 'price' => $price, 'property' => $property, 'property_type' => $property_type];
                }

                $count++;
            });
        }

        $data['viewed'] = $viewedListing;
        $data['saved'] = $savedListing;
        return $data;
    }

    public function crawlFollowUps($id) {
        $count = 0;
        $followUps = [];
        $crawler = $this->client->request('GET', 'https://leads.realgeeks.com/leads/' . $id . '/load_follow_ups');
        $html = json_decode($this->client->getResponse()->getContent(), TRUE);

        $upsHtml = new Crawler($html['follow_ups']);

        if ($upsHtml->filter('.event-row')->count() > 0) {

            $upsHtml->filter('.event-row')->each(function($node) use(&$count, &$followUps) {
                $followUps[$count]['date'] = ($node->filter('.event-container__date > time')->count() > 0) ? trim($node->filter('.event-container__date > time')->attr('title')) : '';
                $followUps[$count]['purpose'] = ($node->filter('.event-container__column--row li:nth-child(2)')->count() > 0) ? trim($node->filter('.event-container__column--row li:nth-child(2)')->text()) : '';
                $followUps[$count]['title'] = ($node->filter('.event-container__column--row li:nth-child(3)')->count() > 0) ? trim($node->filter('.event-container__column--row li:nth-child(3)')->text()) : '';
                $count++;
            });
        }
        $this->output_array['follow_ups'] = $followUps;
    }

}
