<?php

namespace App\Console\Commands;

use App\Traits\FileProcessTrait;
use Exception;
use Illuminate\Console\Command;
use GuzzleHttp\Client;
use Symfony\Component\DomCrawler\Crawler;

class scrap_snippets extends Command
{
    use FileProcessTrait;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'scrap_snippets:exc';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    private $headers =  [
        ':authority' => 'www.breckenridgeassociates.com',
        'accept' => 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9',
        'cookie' => 'rew-auth=5d984e23a32f0f950b133a4c96dcccd0f205d834; PHPSESSID=b1edcd288d0cb147d0da5be2fc0d6f9b'
    ];
    private $base_url = "https://www.breckenridgeassociates.com/backend/";

    private $folder = "brecken/snippets";
    private $data_file = "brecken_snippets";
    private $data_file_urls = "urls_brecken_snippets";
    private $error_file = "error_brecken_snippets";

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info('=========== Starting =============== ');

        $this->data_file = $this->create_json($this->folder, $this->data_file);
        $this->data_file_urls = $this->create_json($this->folder, $this->data_file_urls);
        $this->error_file = $this->create_json($this->folder, $this->error_file);

        $this->scrapCmsSnippets();
        $this->scrapFormSnippets();
        $this->scrapIdxSnippets();
    }

    public function scrapIdxSnippets()
    {
        $this->info('****** Scrapping Idx snippets ********** ');
        for ($i = 1; $i < 8; $i++) {
            $url = $this->base_url . "cms/snippets/?filter=idx&p=" . $i;
            $this->scrapLists($url,$type="idx");
        }
    }

    public function scrapCmsSnippets()
    {
        $this->info('****** Scrapping CMS snippets ********** ');
        for ($i = 1; $i < 3; $i++) {
            $url = $this->base_url . "cms/snippets/?filter=cms&p=" . $i;
            $this->scrapLists($url);
        }
    }


    public function scrapFormSnippets()
    {
        $this->info('****** Scrapping Form snippets ********** ');
        $url = $this->base_url . "cms/snippets/?filter=form";
        $this->scrapLists($url);
    }

    public function scrapLists($url,$type=null)
    {
        $headers = $this->headers;
        $headers['user-agent'] = \Campo\UserAgent::random();

        $client = new Client();
        $response = $client->request(
            'GET',
            $url,
            ['headers' => $headers]
        );

        $response_html = (string) $response->getBody();

        $crawler = new Crawler($response_html, $this->base_url);

        try {

            $node_values = $crawler->filter('.actions')
                ->each(function (Crawler $node, $i) use ($type) {
                    $link = $node->filter('a')->attr('href');
                    if (!empty($link)) {
                        sleep(60);
                        $this->scrapCmsSnippetsDetails($link,$type);
                    }
                });
        } catch (Exception $ex) {
            print_r($ex->getMessage());

            $err_data = [
                $ex->getMessage()
            ];
            $this->saveJsonToFile(
                json_encode($err_data, JSON_PRETTY_PRINT),
                $this->error_file,
                $this->folder
            );
        }
    }

    public function scrapCmsSnippetsDetails($link_id,$type=null)
    {

        $this->info('**** Scrapping page details , id - '. $link_id);

        /////saving urls
        $this->saveJsonToFile(
            json_encode($link_id, JSON_PRETTY_PRINT),
            $this->data_file_urls,
            $this->folder
        );

        try {
            $url = "https://www.breckenridgeassociates.com/backend/cms/snippets/" . $link_id;

            if(!empty($type)){
                $url = "https://www.breckenridgeassociates.com/backend/idx/snippets/" . $link_id;
            }
            
            $headers = $this->headers;
            $headers['user-agent'] = \Campo\UserAgent::random();

            $client = new Client();
            $response = $client->request(
                'GET',
                $url,
                ['headers' => $headers]
            );

            $response_html = (string) $response->getBody();

            $crawler = new Crawler($response_html, $this->base_url);

            ////////
            if(empty($type)){
                $form = $crawler->filter('.rew_check')->form();
            }else{
                $form = $crawler->filter('#idx-builder-form')->form();
            }
            //////
            
            $values = $form->getValues();

            $this->saveJsonToFile(
                json_encode($values, JSON_PRETTY_PRINT),
                $this->data_file,
                $this->folder
            );
        } catch (Exception $ex) {
            print_r($ex->getMessage());

            $err_data = [
                $link_id,
                $ex->getMessage()
            ];
            $this->saveJsonToFile(
                json_encode($err_data, JSON_PRETTY_PRINT),
                $this->error_file,
                $this->folder
            );
        }
    }
}
