<?php

namespace App\Console\Commands;

use App\Traits\FileProcessTrait;
use GuzzleHttp\Client;
use Exception;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Storage;
use Symfony\Component\DomCrawler\Crawler;

class scrap_page extends Command
{
    use FileProcessTrait;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'scrap_page:exc';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    private $headers =  [
        ':authority' => 'www.breckenridgeassociates.com',
        'accept' => 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9',
        'cookie' => 'rew-auth=5d984e23a32f0f950b133a4c96dcccd0f205d834; PHPSESSID=822fa501016dba7f673f6cc1589d8d27'
    ];

    private $base_url = "https://www.breckenridgeassociates.com/backend/cms/pages/";
    private $page_relative_paths = [];

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $folder_name = "brecken";
        $data_file_name = "brecken_home_page";
        // $data_file_name = "brecken_pages";
        // $scraped_urls = "brecken_scraped_urls";
        // $failed_scraped_urls = "brecken_failed_urls";

        $data_file_name = $this->create_json($folder_name, $data_file_name);
        // $scraped_urls = $this->create_json($folder_name, $scraped_urls);
        // $failed_scraped_urls = $this->create_json($folder_name, $failed_scraped_urls);

        $this->info('=========== Starting =============== ');

        // $this->scrap_page_entries_urls();

        $this->scrap_home_page($folder_name, $data_file_name);

        // $this->scrap_page_details($folder_name,$data_file_name,$scraped_urls,$failed_scraped_urls);

        // $this->info('Saving Data');
        // $this->saveJsonToFile(
        //     json_encode($this->page_relative_paths, JSON_PRETTY_PRINT),
        //     $data_file_name,
        //     $folder_name
        // );

    }

    public function scrap_home_page($folder_name, $data_file_name)
    {
        try {
            $url = "https://www.breckenridgeassociates.com/backend/cms/";

            $headers = $this->headers;
            $headers['user-agent'] = \Campo\UserAgent::random();



            $client = new Client();
            $response = $client->request(
                'GET',
                $url,
                ['headers' => $headers]
            );

            $response_html = (string) $response->getBody();

            $crawler = new Crawler($response_html, $this->base_url);

            $form = $crawler->filter('.rew_check')->form();
            $values = $form->getValues();

            if (isset($values['category_html']) && !empty($values['category_html'])) {
                $this->searchImage($values['category_html']);
            }

            $this->saveJsonToFile(
                json_encode($values, JSON_PRETTY_PRINT),
                $data_file_name,
                $folder_name
            );
        } catch (Exception $ex) {
            print_r($ex->getMessage());
        }
    }

    public function scrap_page_details($folder_name, $data_file_name, $scraped_urls, $failed_scraped_urls)
    {
        $urls = $this->load_Json('brecken', 'brecken_page_url_1583393356.json');

        $i = 1;
        foreach ($urls[0] as $key => $url) {

            if ($i == 2) {
                exit;
            }
            $i++;

            sleep(60);

            $this->info('**** Reamining data ' . count($urls[0]) . " of - " . ($key + 1));

            try {
                $relative_url = $url;
                $url = $this->base_url . $url;

                $headers = $this->headers;
                $headers['user-agent'] = \Campo\UserAgent::random();



                $client = new Client();
                $response = $client->request(
                    'GET',
                    $url,
                    ['headers' => $headers]
                );

                $response_html = (string) $response->getBody();

                $crawler = new Crawler($response_html, $this->base_url);

                $form = $crawler->filter('.rew_check')->form();
                $values = $form->getValues();

                if (isset($values['category']) && !empty($values['category'])) {
                    $fileter_select_tag_Val = 'option[value="' . $values['category'] . '"]';
                    $select_value = $crawler->filter('select')->filter($fileter_select_tag_Val)->count() > 0 ? $crawler->filter('select')->filter($fileter_select_tag_Val)->text() : "";

                    $values['category_txt'] = $select_value;
                }

                if (isset($values['category_html']) && !empty($values['category_html'])) {
                    $this->searchImage($values['category_html']);
                }

                $this->saveJsonToFile(
                    json_encode($values, JSON_PRETTY_PRINT),
                    $data_file_name,
                    $folder_name
                );
                $this->saveJsonToFile(
                    json_encode($relative_url, JSON_PRETTY_PRINT),
                    $scraped_urls,
                    $folder_name
                );
            } catch (Exception $ex) {

                $this->saveJsonToFile(
                    json_encode($relative_url, JSON_PRETTY_PRINT),
                    $failed_scraped_urls,
                    $folder_name
                );
                print_r($ex->getMessage());
            }
        }
    }


    public function searchImage($category_html)
    {
        $this->info("Searching Img");
        try {
            $crawler = new Crawler($category_html);

            $node_values = $crawler->filter('img')->each(function (Crawler $node, $i) {

                return [
                    'url' => 'https://www.breckenridgeassociates.com/' . $node->attr('src'),
                    'img_name' => $node->attr('src')
                ];
            });

            foreach ($node_values as $key => $value) {

                $url = $value['url'];
                $name = $value['img_name'];

                $file = file_get_contents($url);
                Storage::disk('public')->put($name, $file);
            }
        } catch (Exception $ex) {
            print_r($ex->getMessage());
        }
    }

    // public function scrap_page_entries_urls()
    // {
    //     $this->info('*************************************');
    //     $this->info('Scraping Blog Entries');

    //     $blog_post_urls = [];

    //     try {

    //         $url = $this->base_url;

    //         $headers = $this->headers;
    //         $headers['user-agent'] = \Campo\UserAgent::random();


    //         $client = new Client();
    //         $response = $client->request(
    //             'GET',
    //             $url,
    //             ['headers' => $headers]
    //         );

    //         $response_html = (string) $response->getBody();

    //         $crawler = new Crawler($response_html);

    //         $node_values = $crawler->filter(".item_content_title")->each(function (Crawler $node, $i) {
    //             array_push($this->page_relative_paths, $node->filter('a')->attr('href'));
    //         });

    //         $blog_post_urls[] = $node_values;
    //     } catch (Exception $ex) {
    //         print_r($ex->getMessage());
    //     }

    //     return;
    // }
}
