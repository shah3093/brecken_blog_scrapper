<?php

namespace App\Console\Commands;

use App\Traits\FileProcessTrait;
use Exception;
use Illuminate\Console\Command;
use Symfony\Component\DomCrawler\Crawler;

class replace_img extends Command
{
    use FileProcessTrait;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'replace_img:exc';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Replace image src with RG image src';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $json_datas = $this->load_Json('brecken/page', 'brecken_pages_1583508751.json');
        // $json_datas = $this->load_Json('brecken/page', 'brecken_snippets_1583915188.json');
        // $json_datas = $this->load_Json('brecken/new', 'blogs_brecken_1583919445.json');
        // $new_file = $this->create_json('brecken/new', 'snippets_brecken');
        $new_file = $this->create_json('brecken/new', 'page_brecken');
        // $img_src = $this->create_json('brecken/new', 'img_src');



        foreach ($json_datas as $key => $value) {
            $this->info("Remainig Data " . count($json_datas) . " of " . ($key + 1));
            $this->searchImage($value, $new_file);
            // $this->showImgSrc($value, $img_src);
        }
    }

    public function showImgSrc($value, $new_file)
    {
        if (!isset($value['category_html'])) {
            return;
        }

        // $crawler = new Crawler($value['category_html']);
        $crawler = new Crawler($value['category_html']);

        $node_values = $crawler->filter('img')->each(function (Crawler $node, $i) {
            $this->info($node->attr('src'));
            return $node->attr('src');
        });

        $this->saveJsonToFile(
            json_encode($node_values, JSON_PRETTY_PRINT),
            $new_file,
            'brecken/new'
        );
    }


    public function searchImage($value, $new_file)
    {
        if (!isset($value['category_html'])) {
            return;
        }
        
        $value['category_html'] = $value['category_html'];

        $this->info("Searching Img");
        try {

            $node_values = "";
            $base_urls = "https://u.realgeeks.media/breckenridgeassociates";

            if (!empty($value['category_html'])) {
                $tmp_body = $category_html = $value['category_html'];
                $crawler = new Crawler($category_html);

                $node_values = $crawler->filter('img')->each(function (Crawler $node, $i) use ($base_urls) {

                    $img_src = $node->attr('src');

                    if (strpos($img_src, "http") !== false) {
                        $new_src = $node->attr('src');
                        $this->info($new_src);
                    } else {
                        $new_src = str_replace('agent-1', 'agent_1', $node->attr('src'));
                        $new_src = str_replace(' ', '_', $new_src);
                        $new_src = $base_urls . $new_src;

                        $this->info($new_src);
                    }


                    return [
                        'new_src' => $new_src,
                        'old_src' => $node->attr('src')
                    ];
                });
            }

            $new_body = $value['category_html'];
            if (!empty($node_values)) {
                foreach ($node_values as $key => $nvalue) {
                    $new_body = str_replace($nvalue['old_src'], $nvalue['new_src'], $new_body);
                }
            }

            // $value['category_html'] = $new_body;
            $value['category_html'] = $new_body;
            $this->saveJsonToFile(
                json_encode($value, JSON_PRETTY_PRINT),
                $new_file,
                'brecken/new'
            );
        } catch (Exception $ex) {
            print_r($ex->getMessage());
        }
    }
}
