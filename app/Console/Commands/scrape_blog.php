<?php

namespace App\Console\Commands;

use App\Traits\FileProcessTrait;
use GuzzleHttp\Client;
use Exception;
use Illuminate\Console\Command;
use Symfony\Component\DomCrawler\Crawler;

class scrape_blog extends Command
{
    use FileProcessTrait;
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'scrape_blog:exc';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    private $headers =  [
        ':authority' => 'www.breckenridgeassociates.com',
        'accept' => 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9',
        'cookie' => 'rew-auth=5d984e23a32f0f950b133a4c96dcccd0f205d834; PHPSESSID=822fa501016dba7f673f6cc1589d8d27; results-view=grid; GCLB=CKaVh6aCpPySzgE'
    ];

    private $base_url = "https://www.breckenridgeassociates.com/backend/blog/entries/";
    private $blog_relative_paths = [];

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $folder_name = "brecken";
        $data_file_name = "brecken";

        $data_file_name = $this->create_json($folder_name, $data_file_name);

        $this->info('=========== Starting =============== ');

        $this->scrap_blog_entries();
        $this->scrap_blog_details($folder_name,$data_file_name);
    }


    public function scrap_blog_details($folder_name,$data_file_name)
    {
        $this->info('*************************************');
        $this->info('Scraping Blog Details');


        $i = 1;
        foreach ($this->blog_relative_paths as $key => $relative_path) {

            $this->info('**** Reamining data '.count($this->blog_relative_paths)." of - ".($key+1));

            if($i == 5){
                return;
            }

            sleep(60);

            $i++;

            try {
                $url = $this->base_url . $relative_path;

                $headers = $this->headers;
                $headers['user-agent'] = \Campo\UserAgent::random();



                $client = new Client();
                $response = $client->request(
                    'GET',
                    $url,
                    ['headers' => $headers]
                );

                $response_html = (string) $response->getBody();

                $crawler = new Crawler($response_html, $this->base_url);

                $form = $crawler->filter('.rew_check')->form();
                $values = $form->getValues();

                $this->saveJsonToFile(
                    json_encode($values, JSON_PRETTY_PRINT),
                    $data_file_name,
                    $folder_name
                );

            } catch (Exception $ex) {
                print_r($ex->getMessage());
            }
        }
    }


    public function scrap_blog_entries()
    {
        $this->info('*************************************');
        $this->info('Scraping Blog Entries');

        $blog_post_urls = [];

        try {

            for ($i = 1; $i <= 31; $i++) {

                $this->info("Remainig Data 31 of ".$i);

                if ($i == 2) {
                    return ;
                }

                $url = $this->base_url . "?p=" . $i;

                $headers = $this->headers;
                $headers['user-agent'] = \Campo\UserAgent::random();


                $client = new Client();
                $response = $client->request(
                    'GET',
                    $url,
                    ['headers' => $headers]
                );

                $response_html = (string) $response->getBody();

                $crawler = new Crawler($response_html);

                $node_values = $crawler->filter(".item_content_title")->each(function (Crawler $node, $i) {
                    array_push($this->blog_relative_paths, $node->filter('a')->attr('href'));
                });

                $blog_post_urls[] = $node_values;
            }
        } catch (Exception $ex) {
            print_r($ex->getMessage());
        }

        return ;
    }
}
