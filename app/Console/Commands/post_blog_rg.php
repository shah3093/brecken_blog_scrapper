<?php

namespace App\Console\Commands;

use App\Traits\FileProcessTrait;
use App\Traits\LoginTrait;
use Exception;
use Illuminate\Console\Command;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7\MultipartStream;
use Symfony\Component\BrowserKit\History;
use Symfony\Component\DomCrawler\Crawler;

class post_blog_rg extends Command
{
    use FileProcessTrait, LoginTrait;
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'post_blog_rg:exc';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    private $slugs = [];

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();

        $jar = new \GuzzleHttp\Cookie\CookieJar();
        $this->client = new client(['cookies' => $jar]);
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info('=========== Starting =============== ');

        $email = "Nathan@breckenridgeassociates.com";
        $password = "BreckAss1";

        $this->loginToRG($email, $password);

        $json_data = $this->load_Json('brecken/new', 'blogs_brecken_1583919445.json');
        $failed_blog_ids = $this->create_json('brecken', 'brecken_failed_blog_ids');

        // $not_posted = [
        //     '847', '867', '787', '781', '767', '789'
        // ];

        foreach ($json_data as $key => $data) {
            $this->info("Remainig Data " . count($json_data) . " of " . ($key + 1));

            // if (in_array($data['id'], $not_posted)) {
            //     $this->postToBLog($data, $failed_blog_ids);
            // }

            $this->postToBLog($data, $failed_blog_ids);
            // exit;
        }
    }

    public function generateSlug($title)
    {
        $gtitle = $title . rand(1, 10);
        $slug = str_slug($gtitle, '-');

        if (in_array($slug, $this->slugs)) {
            $this->generateSlug($title);
        }

        return $slug;
    }

    public function postToBLog($data, $failed_blog_ids)
    {
        $csrf = $this->getCsrf();
        $publish_date = date('Y-m-d', strtotime($data['timestamp_published']));
        $publish_time = date('H:m:s', strtotime($data['timestamp_published']));


        $categories = 6;
        if (isset($data['categories[0]'])) {
            $categories = 3; // Hot List: Top 5 Real Estate Deals
        } else if (isset($data['categories[1]'])) {
            $categories = 2; // Breckenridge and Summit County Real Estate Statistics
        } else if (isset($data['categories[2]'])) {
            $categories = 4; // Events
        } else if (isset($data['categories[3]'])) {
            $categories = 6; // New Listings
        } else if (isset($data['categories[4]'])) {
            $categories = 5; // Real Estate Listing News
        }

        $slug = $this->generateSlug($data['title']);

        $post_datas = [
            'csrfmiddlewaretoken' => $csrf,
            'title' => $data['title'],
            'slug' => $slug,
            'body' => $data['body'],
            'status' => 2,
            'allow_comments' => 'on',
            'nofollow_comments' => 'on',
            'publish_0' => $publish_date,
            'publish_1' => $publish_time,
            'initial-publish_0' => date('Y-m-d', strtotime(now())),
            'initial-publish_1' => date('H:m:s', strtotime(now())),
            'page_title' => $data['title'],
            'meta_description' => $data['meta_tag_desc'],
            'meta_keywords' => $data['meta_tag_keywords'],
            'categories' => $categories,
            '_save' => 'Save'
        ];


        $params = [
            'headers' => [
                'Referer' => 'https://breckenridgeassociates.realgeeks.com/admin/blog/post/add/',
                'Connection' => 'keep-alive',
                'Host' => 'breckenridgeassociates.realgeeks.com',
                'Origin' => 'https://breckenridgeassociates.realgeeks.com',
                'User-Agent' => \Campo\UserAgent::random()
            ],
            'form_params' => $post_datas, // here is all the magic
        ];

        try {
            $this->info("Posting Data");
            $url = "https://breckenridgeassociates.realgeeks.com/admin/blog/post/add/";

            $response = $this->client->request("POST", $url, $params);

            $response =  json_decode($response->getBody(), true);

            $this->info("Success !!");
        } catch (Exception $ex) {
            print_r($ex->getMessage());

            $err_d = [
                $data['id'],
                $ex->getMessage()
            ];

            $this->saveJsonToFile(
                json_encode($err_d, JSON_PRETTY_PRINT),
                $failed_blog_ids,
                "brecken"
            );
        }
    }

    public function getCsrf()
    {
        try {
            $base_url = "https://breckenridgeassociates.realgeeks.com/admin";
            $url = "https://breckenridgeassociates.realgeeks.com/admin/blog/post/add/";
            $response = $this->client->request('GET', $url);

            $response_html = (string) $response->getBody();
            $crawler = new Crawler($response_html, $base_url);

            $form = $crawler->filter('#post_form')->form();
            $values = $form->getValues();

            return $values['csrfmiddlewaretoken'];
        } catch (Exception $ex) {
            print_r($ex->getMessage());
        }
    }
}
