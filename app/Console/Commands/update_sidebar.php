<?php

namespace App\Console\Commands;

use App\Traits\FileProcessTrait;
use App\Traits\LoginTrait;
use Exception;
use Illuminate\Console\Command;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7\MultipartStream;
use Symfony\Component\BrowserKit\History;
use Symfony\Component\DomCrawler\Crawler;
use Illuminate\Support\Str;

class update_sidebar extends Command
{
    use FileProcessTrait, LoginTrait;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'update_sidebar:exc';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();

        $this->jar = new \GuzzleHttp\Cookie\CookieJar();
        $this->client = new client(['cookies' => $this->jar]);
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info('=========== Starting =============== ');

        $email = "featuredmarketing@gmail.com";
        $password = "Q!wfSc7aUu%y";

        $this->loginToRG($email, $password);

        $sidebars = $this->scrap_sidebars();

        // $sidebars = [
        //     'Maynardville' => 'admin/content/sidebar/42/change/',
        //     'Morristown' => 'admin/content/sidebar/71/change/',
        //     'Artemus' => 'admin/content/sidebar/77/change/',
        //     'Barbourville' => 'admin/content/sidebar/78/change/',
        //     'Bean Station' => 'admin/content/sidebar/79/change/',
        //     'Cannon' => 'admin/content/sidebar/81/change/',
        //     'Corbin' => 'admin/content/sidebar/82/change/',
        //     'Gray' => 'admin/content/sidebar/85/change/',
        //     'Pigeon Forge' => 'admin/content/sidebar/94/change/'
        // ];

        // $sidebar_url = "admin/content/sidebar/5/change/";
        // $backup_sidebar = $this->cloneContent($sidebar_url);
        $backup_sidebar = "";

        // dd($backup_sidebar);

        $sidebar_cities = [];

        $this->updateSidebar($sidebars, $sidebar_cities, $backup_sidebar);
    }

    public function updateSidebar($sidebars, $sidebar_cities, $backup_sidebar = null)
    {
        // dd($backup_sidebar);

        // $backup_sidebaritem_4 = $backup_sidebar['sidebaritem_set-4-sidebar_links'];
        // $backup_sidebaritem_5 = $backup_sidebar['sidebaritem_set-5-sidebar_links'];

        foreach ($sidebars as  $sidebar_name => $sidebar) {

            // $sidebar_url = "admin/content/sidebar/98/change/";
            $sidebar_url = $sidebar['sidebar_link'];

            // $sidebar_id = (int) filter_var($sidebar_url, FILTER_SANITIZE_NUMBER_INT);


            $sidebar_data = $this->cloneContent($sidebar_url);
            // $sidebar_data = "";

            $contents = $sidebar_data;

            // dd($contents);

            if(strpos($contents['sidebaritem_set-4-sidebar_links'],"NC by Price") !== false){
                $links_4 = $contents['sidebaritem_set-4-sidebar_links'];
                $contents['sidebaritem_set-4-sidebar_links'] = str_replace("-sidebar","",$links_4);
            }else{
                $this->info("-------".$sidebar['sidebar_name']." Link 4 -------");
            }
            
            if(strpos($contents['sidebaritem_set-5-sidebar_links'],"NC by Features") !== false){
                $links_5 = $contents['sidebaritem_set-5-sidebar_links'];
                $contents['sidebaritem_set-5-sidebar_links'] = str_replace("-sidebar","",$links_5);
            }else{
                $this->info("-------".$sidebar['sidebar_name']." Link 5 -------");
            }

            

            // $city_name = trim($sidebar['sidebar_name'], "City Sidebar");
            // $city_name = str_replace("City Sidebar","",$sidebar['sidebar_name']);
            // // $city_name = "C".trim($sidebar['sidebar_name'], "City Sidebar");
            // $city_name = trim($city_name);

            // $this->info($city_name);

            // $city_slug = str_slug($city_name);
            // $city_slug = Str::slug($city_name);

            // dd($city_slug);

            // $link_10 = str_replace("Almond", $city_name, $backup_sidebar['sidebaritem_set-5-sidebar_links']);
            // $contents['sidebaritem_set-10-sidebar_links'] = str_replace("almond", $city_slug, $link_10);
            // $contents['sidebaritem_set-10-order'] = 3;
            // $contents['sidebaritem_set-10-wysiwyg_content'] = $backup_sidebar['sidebaritem_set-5-wysiwyg_content'];
            // $contents['sidebaritem_set-10-sidebar'] = $sidebar_id;


            // $link_11 = str_replace("Almond", $city_name, $backup_sidebaritem_5);
            // $contents['sidebaritem_set-11-sidebar_links'] = str_replace("almond", $city_slug, $link_11);
            // $contents['sidebaritem_set-11-order'] = 3;
            // $contents['sidebaritem_set-11-wysiwyg_content'] = $backup_sidebar['sidebaritem_set-5-wysiwyg_content'];
            // $contents['sidebaritem_set-11-sidebar'] = $sidebar_id;

           


            // dd($contents['sidebaritem_set-3-sidebar_links']);

            //// repalicing links
            // $exploded_strs = explode("\r\n", $contents['sidebaritem_set-3-sidebar_links']);
            // $set_3_links = [];
            // foreach ($exploded_strs as $key => $exploded_str) {
            //     if (!empty($exploded_str) && strpos($exploded_str, "||") !== false) {
            //         $str = explode("||", $exploded_str);
            //         $links_contents_arr = explode("/", $str[0]);

            //         $revert_link = "";
            //         if (strpos($str[1], "A Great Room") !== false) {
            //             if (strpos($str[0], "with-great-rooms") !== false) {
            //                 $revert_link = $str[0];
            //             } else if (strpos($str[0], "with-great-room") !== false) {
            //                 $revert_link = str_replace("with-great-room", "with-great-rooms", $str[0]);
            //             } else {
            //                 $revert_link = $str[0] . "/with-great-rooms/";
            //             }
            //         } else {
            //             $revert_link = $str[0];
            //         }

            //         // $links_contents_arr_len = count($links_contents_arr);
            //         // $i = 0;

            //         // $revert_link = "";
            //         // foreach ($links_contents_arr as  $links_content) {
            //         //     if ($i == ($links_contents_arr_len - 2)) {

            //         //         if (strpos($links_content, "with") !== false) {
            //         //             $revert_link .= "/" . $links_content;
            //         //         } else {


            //         //             if($links_content == "s"){
            //         //                 $revert_link .= "/with-great-rooms";
            //         //             }else{
            //         //                 $revert_link .= "/with-" . $links_content;
            //         //             }

            //         //             // if ("great-room" == $links_content) {
            //         //             //     $revert_link .= "/with-great-rooms";
            //         //             // }else{
            //         //             //     $revert_link .= "/with-" . $links_content;
            //         //             // }
            //         //         }
            //         //     } else {
            //         //         $revert_link .= "/" . $links_content;
            //         //     }

            //         //     $i++;
            //         // }


            //         // $revert_link = str_replace("with-great-room/", "", $revert_link);
            //         $revert_link = str_replace("//", "/", $revert_link);
            //         $set_3_links[] = [
            //             $str[0] => $revert_link
            //         ];
            //     }
            // }

            // // dd($set_3_links);

            // $set3_sidebar_links = $contents['sidebaritem_set-3-sidebar_links'];
            // foreach ($set_3_links as $links) {
            //     foreach ($links as $s3key => $s3link) {
            //         $set3_sidebar_links = str_replace($s3key . "||", $s3link . "||", $set3_sidebar_links);
            //     }
            // }

            // $this->info($set3_sidebar_links);
            // exit;

            // $contents['sidebaritem_set-3-sidebar_links'] = $set3_sidebar_links;

            // dd($set3_sidebar_links);
            //// repalicing links

            // unset($contents['sidebaritem_set-2-sidebar_links']);
            // unset($contents['sidebaritem_set-6-sidebar_links']);

            // $contents['sidebaritem_set-4-DELETE'] = "on";
            // $contents['sidebaritem_set-5-DELETE'] = "on";


            // $link_2 = "===Browse " . $sidebar_name . " Subdivisions\r\n";
            // $link_6 = "===" . $sidebar_7_header[$sidebar_name] . "||" . $sidebar_name . " Market Reports\r\n";



            // foreach ($sidebar_cities[$sidebar_name] as $city => $city_url) {
            //     $link_2 .= $city_url['sidebar_item_3'] . "||" . $city_url['ancohor_text'] . "\r\n";
            //     $link_6 .= $city_url['sidebar_item_7'] . "||" . $city_url['ancohor_text'] . "\r\n";
            // }

            // $contents['sidebaritem_set-6-sidebar_links'] = $link_6;
            // $contents['sidebaritem_set-2-sidebar_links'] = $link_2;

            try {
                $url = "https://www.smokymountainhomes4sale.com/" . $sidebar_url;

                // dd($contents);

                $response = $this->client->request("POST", $url, [
                    'form_params' => $contents,
                    'headers' => [
                        'Referer' => $url,
                        'Connection' => 'keep-alive',
                        'Host' => 'www.smokymountainhomes4sale.com',
                        'Origin' => 'https://www.smokymountainhomes4sale.com',
                        'User-Agent' => \Campo\UserAgent::random()
                    ],
                ]);



                $this->info("Success !!");

                // exit;
            } catch (Exception $ex) {
                print_r($ex->getMessage());
            }
        }
    }

    public function cloneContent($url)
    {
        $this->info("Getting Clone Content " . $url);

        try {

            $base_url = "https://www.smokymountainhomes4sale.com/admin";

            $site_url = "https://www.smokymountainhomes4sale.com/";

            $response = $this->client->request('GET', $site_url . $url);

            $response_html = (string) $response->getBody();
            $crawler = new Crawler($response_html, $base_url);

            $form = $crawler->filter('#sidebar_form')->form();
            $values = $form->getValues();

            return $values;
        } catch (Exception $ex) {
            print_r($ex->getMessage());
        }
    }

    public function scrap_sidebars()
    {

        $sidebars_name = [
            // 'Almond City Sidebar',
            // 'Balsam City Sidebar',
            'Barkers Creek City Sidebar',
            // 'Bryson City Sidebar',
            // 'Canada City Sidebar',
            // 'Canton City Sidebar',
            // 'Cashiers City Sidebar',
            // 'Cherokee (Jackson Co.) City Sidebar',
            // 'Cherokee (Swain Co.) City Sidebar',
            // 'Clyde City Sidebar',
            // 'Cullowhee City Sidebar',
            // 'Dillsboro City Sidebar',
            // 'Franklin City Sidebar',
            // 'Glenville City Sidebar',
            // 'Hayesville City Sidebar',
            // 'Highlands City Sidebar',
            // 'Maggie Valley City Sidebar',
            // 'Murphy City Sidebar',
            // 'Otto City Sidebar',
            // 'Qualla City Sidebar',
            // 'Robbinsville City Sidebar',
            // 'Scaly Mountain City Sidebar',
            // 'Sky Valley City Sidebar',
            // 'Stecoah City Sidebar',
            // 'Sylva City Sidebar',
            // 'Topton City Sidebar',
            // 'Tuckasegee City Sidebar',
            // 'Waynesville City Sidebar',
            // 'Webster City Sidebar',
            // 'Whittier City Sidebar'
        ];

        // echo count($sidebars_name);
        // exit;


        $base_url = "https://www.smokymountainhomes4sale.com/admin";

        $sidebars = [];
        $new_sidebar  = [];

        for ($i = 0; $i < 1; $i++) {
            // $this->info($i);

            $url = "https://www.smokymountainhomes4sale.com/admin/content/sidebar/";


            $response = $this->client->request('GET', $url);

            $response_html = (string) $response->getBody();
            $crawler = new Crawler($response_html, $base_url);

            $node_values = $crawler->filter('.field-name')->each(function (Crawler $node, $i) use ($sidebars_name) {
                $url_name = trim($node->text());

                if (in_array($url_name, $sidebars_name)) {

                    $data = [
                        'sidebar_name' => $url_name,
                        'sidebar_link' => $node->filter('a')->attr('href')
                    ];

                    return $data;
                }
            });

            foreach ($node_values as $key => $value) {
                if (isset($value['sidebar_name'])) {
                    array_push($new_sidebar, $value['sidebar_name']);
                    array_push($sidebars, $value);
                }
            }
        }

        // dd($sidebars);

        return $sidebars;
    }
}
