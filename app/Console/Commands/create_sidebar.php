<?php

namespace App\Console\Commands;

use App\Traits\FileProcessTrait;
use App\Traits\LoginTrait;
use Exception;
use Illuminate\Console\Command;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7\MultipartStream;
use Symfony\Component\BrowserKit\History;
use Symfony\Component\DomCrawler\Crawler;

class create_sidebar extends Command
{
    use FileProcessTrait, LoginTrait;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'create_sidebar:exc';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    public $clone_content = [];

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();

        $this->jar = new \GuzzleHttp\Cookie\CookieJar();
        $this->client = new client(['cookies' => $this->jar]);
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info('=========== Starting =============== ');

        $email = "featuredmarketing@gmail.com";
        $password = "Q!wfSc7aUu%y";

        $this->loginToRG($email, $password);

        $this->clone_content = $this->getCloneContent();
        // dd($this->clone_content);
        $this->savingSidebarContent();
    }

    public function savingSidebarContent()
    {


        $cities = [
            'Dayton',
            'Harriman',
            'Kingston',
            'Midtown',
            'Oak Ridge',
            'Oliver Springs',
            'Rockwood',
            'Spring',
            'Ten Mile'
        ];

        foreach ($cities as $key => $city) {

            $this->info("Saving City " . $city);

            $city_slug = str_slug($city);
            $city_query_string = str_replace(" ", "%20", trim($city));

            $duplicate_content = $this->clone_content;

            $small_county = "roane";
            $capital_county = "Roane";

            // dd($duplicate_content);

            $duplicate_content['name'] = str_replace("Dandridge Area", $city . " City", $duplicate_content['name']);
            $duplicate_content['name'] = str_replace("Jefferson", $capital_county, $duplicate_content['name']);

            // $duplicate_content['sidebaritem_set-1-sidebar_links'] = str_replace("Jefferson", $city, $duplicate_content['sidebaritem_set-1-sidebar_links']);
            // $duplicate_content['sidebaritem_set-1-sidebar_links'] = str_replace("jefferson", $city_slug, $duplicate_content['sidebaritem_set-1-sidebar_links']);

            $duplicate_content['sidebaritem_set-2-sidebar_links'] = str_replace("Dandridge", $city, $duplicate_content['sidebaritem_set-2-sidebar_links']);

            $duplicate_content['sidebaritem_set-2-sidebar_links'] = str_replace("jefferson", $small_county, $duplicate_content['sidebaritem_set-2-sidebar_links']);
            $duplicate_content['sidebaritem_set-2-sidebar_links'] = str_replace("dandridge", $city_slug, $duplicate_content['sidebaritem_set-2-sidebar_links']);
            $duplicate_content['sidebaritem_set-2-sidebar_links'] = str_replace("dandrige", $city_slug, $duplicate_content['sidebaritem_set-2-sidebar_links']);

            $duplicate_content['sidebaritem_set-3-sidebar_links'] = str_replace("Dandridge", $city, $duplicate_content['sidebaritem_set-3-sidebar_links']);

            $duplicate_content['sidebaritem_set-3-sidebar_links'] = str_replace("jefferson", $small_county, $duplicate_content['sidebaritem_set-3-sidebar_links']);
            $duplicate_content['sidebaritem_set-3-sidebar_links'] = str_replace("dandridge", $city_slug, $duplicate_content['sidebaritem_set-3-sidebar_links']);

            $duplicate_content['sidebaritem_set-4-sidebar_links'] = str_replace("Dandridge", $city, $duplicate_content['sidebaritem_set-4-sidebar_links']);

            $duplicate_content['sidebaritem_set-4-sidebar_links'] = str_replace("jefferson", $small_county, $duplicate_content['sidebaritem_set-4-sidebar_links']);
            $duplicate_content['sidebaritem_set-4-sidebar_links'] = str_replace("dandridge", $city_slug, $duplicate_content['sidebaritem_set-4-sidebar_links']);

            $duplicate_content['sidebaritem_set-5-sidebar_links'] = str_replace("Dandridge", $city, $duplicate_content['sidebaritem_set-5-sidebar_links']);

            $duplicate_content['sidebaritem_set-5-sidebar_links'] = str_replace("jefferson", $small_county, $duplicate_content['sidebaritem_set-5-sidebar_links']);
            $duplicate_content['sidebaritem_set-5-sidebar_links'] = str_replace("dandridge", $city_slug, $duplicate_content['sidebaritem_set-5-sidebar_links']);



            $duplicate_content['sidebaritem_set-6-sidebar_links'] = str_replace("Dandridge Market", $city . " Market", $duplicate_content['sidebaritem_set-6-sidebar_links']);
            $duplicate_content['sidebaritem_set-6-sidebar_links'] = str_replace("Dandridge Log", $city . " Log", $duplicate_content['sidebaritem_set-6-sidebar_links']);
            $duplicate_content['sidebaritem_set-6-sidebar_links'] = str_replace("Jefferson", $capital_county, $duplicate_content['sidebaritem_set-6-sidebar_links']);
            $duplicate_content['sidebaritem_set-6-sidebar_links'] = str_replace("Dandridge", $city_query_string, $duplicate_content['sidebaritem_set-6-sidebar_links']);

            // dd($duplicate_content);

            try {
                $url = "https://www.anniemaloney.com/admin/content/sidebar/add/";

                $response = $this->client->request("POST", $url, [
                    'form_params' => $duplicate_content,
                    'headers' => [
                        'Referer' => 'https://www.anniemaloney.com/admin/content/sidebar/add/',
                        'Connection' => 'keep-alive',
                        'Host' => 'www.anniemaloney.com',
                        'Origin' => 'https://www.anniemaloney.com',
                        'User-Agent' => \Campo\UserAgent::random()
                    ],
                ]);

                $this->info("Success !!");
            } catch (Exception $ex) {
                print_r($ex->getMessage());
            }
        }
    }

    public function getCloneContent()
    {
        $this->info("Getting Clone Content");

        try {
            $base_url = "https://www.anniemaloney.com/admin";
            $url = "https://www.anniemaloney.com/admin/content/sidebar/12/change/clone/";
            $response = $this->client->request('GET', $url);

            $response_html = (string) $response->getBody();
            $crawler = new Crawler($response_html, $base_url);

            $form = $crawler->filter('#sidebar_form')->form();
            $values = $form->getValues();

            return $values;
        } catch (Exception $ex) {
            print_r($ex->getMessage());
        }
    }
}
